package com.manman.common.web;

import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.manman.common.service.ApiService;
import com.manman.common.util.CommonUtil;

import lombok.Setter;

@ConfigurationProperties(prefix = "manman.api")
@Controller
public class ApiController {
	
	final private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Setter 
	private String url;
	@Setter 
	private String port;
	@Setter 
	private String ip;
	
	@Autowired
    private ApiService apiService;
	
	@PostMapping(value = "/api", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<Object> callApiFormRequest(@RequestParam Map<String, Object> requestMap, HttpServletRequest request) throws Exception {
		
		// 도메인 검증 추가!!
		// 개발일 때 ip 체크, 운영일 때 도메인 체크, app 일때?
		Boolean isCertificate = false;
		String clientIp = CommonUtil.getClientIpAddr(request);
		String serverName = request.getServerName();
		
		logger.debug("ApiController callApiFormRequest clientIp : " + clientIp);
		logger.debug("ApiController callApiFormRequest serverName : " + serverName);
		
		if(ip.equals(serverName) || "maandoo.com".equals(serverName) || "www.maandoo.com".equals(serverName)) {
			
			isCertificate = true;
			
		}else {
			
			String applicationId = (String) requestMap.get("p");
			logger.debug("ApiController callApiFormRequest applicationId : " + applicationId);
			
			if(applicationId.contains("com.manman.")) {
				isCertificate = true;
			}
			
		}
		
		if(isCertificate) {
			
			logger.debug("ApiController callApiFormRequest requestData : " + requestMap.toString());
			
			String apiUrl = (String) requestMap.get("url");
			
			logger.debug("MainController callApiFormRequest apiUrl : " + apiUrl);
			
			requestMap.remove("url");
			
			logger.debug("MainController callApiFormRequest requestMap : " + requestMap.toString());
			
			StringBuilder urlBuilder = new StringBuilder(url + ":" + port + apiUrl);
			
			Map<String, Object> property = new HashMap<String, Object>();
	    	
	    	Enumeration<String> headerNames = request.getHeaderNames();
	        headerNames.asIterator().forEachRemaining(headerName -> {
	        	logger.debug("MainController callApiFormRequest header info : " + headerName + ": " + request.getHeader(headerName));
	        	property.put(headerName, request.getHeader(headerName));
	        });
	    	
	    	String response = apiService.postApi(urlBuilder.toString(), requestMap, property);
	    	
	    	logger.debug("MainController callApiFormRequest response : " + response);
			
			return ResponseEntity.ok().body(response);
		}else {
			return ResponseEntity.badRequest().body("bad request");
		}
		
	}
	
	@PostMapping(value = "/api", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> callApiJsonRequest(HttpServletRequest request) throws Exception {
		
		// 도메인 검증 추가!!
		// 개발일 때 ip 체크, 운영일 때 도메인 체크, app 일때?
		Boolean isCertificate = false;
		String clientIp = CommonUtil.getClientIpAddr(request);
		
		String serverName = request.getServerName();
		
		logger.debug("ApiController callApiJsonRequest clientIp : " + clientIp);
		logger.debug("ApiController callApiJsonRequest serverName : " + serverName);
		
		ServletInputStream inputStream = request.getInputStream();
        String requestBody = StreamUtils.copyToString(inputStream, StandardCharsets.UTF_8);
        logger.debug("ApiController callApiJsonRequest requestBody : " + requestBody);
        
        JsonObject requestObject = JsonParser.parseString(requestBody).getAsJsonObject();
        
        logger.debug("ApiController callApiJsonRequest requestObject : " + requestObject.toString());
		
        if(ip.equals(serverName) || "maandoo.com".equals(serverName) || "www.maandoo.com".equals(serverName)) {
			
			isCertificate = true;
			
		}else {
			
			String applicationId = requestObject.get("p").getAsString();
			logger.debug("ApiController callApiFormRequest applicationId : " + applicationId);
			
			if(applicationId.contains("com.manman.")) {
				isCertificate = true;
			}
			
		}
		
		if(isCertificate) {
	        
	        String apiUrl = requestObject.get("url").getAsString();
			
			logger.debug("ApiController callApiJsonRequest apiUrl : " + apiUrl);
	        
	        requestObject.remove("url");
	        
			logger.debug("ApiController callApiJsonRequest requestObject : " + requestObject.toString());
	    	
			@SuppressWarnings("unchecked")
			Map<String, Object> requestMap = new ObjectMapper().readValue(requestObject.toString(), Map.class);
			
			logger.debug("ApiController callApiJsonRequest requestMap : " + requestMap.toString());
			
	    	StringBuilder urlBuilder = new StringBuilder(url + ":" + port + apiUrl);
	    	
	    	logger.debug("ApiController callApiJsonRequest urlBuilder : " + urlBuilder.toString());
	    	
	    	Map<String, Object> property = new HashMap<String, Object>();
	    	
	    	Enumeration<String> headerNames = request.getHeaderNames();
	        headerNames.asIterator().forEachRemaining(headerName -> {
	        	logger.debug("ApiController callApiJsonRequest header info : " + headerName + ": " + request.getHeader(headerName));
	        	property.put(headerName, request.getHeader(headerName));
	        });
	    	
	    	//String response = apiService.postApi(urlBuilder.toString(), requestObject, property);
	    	String response = apiService.postApi(urlBuilder.toString(), requestMap, property);
	    	
	    	logger.debug("ApiController callApiJsonRequest response : " + response);
	    	
			return ResponseEntity.ok().body(response);
		}else {
			return ResponseEntity.badRequest().body("bad request");
		}

	}

}
