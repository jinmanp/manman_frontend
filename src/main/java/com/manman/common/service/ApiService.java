package com.manman.common.service;

import java.util.Map;

import com.google.gson.JsonObject;

public interface ApiService {
	/**
	 * API 호출
	 * @param
	 * @return url
	 * @return param
	 * @return resultFilePath 파일 저장경로
	 * @throws Exception
	 */
	public String getApi(String urlBuilder, Map<String, Object> parameter) throws Exception;

	public String getApi(String urlBuilder, Map<String, Object> parameter, Map<String, Object> property) throws Exception;

	public String getApi(String urlBuilder, String parameter, Map<String, Object> property) throws Exception;

	public String postApi(String urlBuilder, Map<String, Object> parameter) throws Exception;

	public String postApi(String urlBuilder, Map<String, Object> parameter, Map<String, Object> property) throws Exception;

	public String postApi(String urlBuilder, String parameter, Map<String, Object> property) throws Exception;

	public String postApi(String urlBuilder, JsonObject parameter, Map<String, Object> property) throws Exception;

}
