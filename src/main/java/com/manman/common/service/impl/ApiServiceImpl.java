package com.manman.common.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;
import com.manman.common.service.ApiService;
import com.manman.common.util.SSLConn;

@Service("apiService")
public class ApiServiceImpl implements ApiService {

	final private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public String getApi(String urlBuilder, Map<String, Object> parameter) throws Exception {
		return getApi(urlBuilder, getParameter(parameter), null);
	}

	@Override
	public String getApi(String urlBuilder, Map<String, Object> parameter, Map<String, Object> property) throws Exception {
		return getApi(urlBuilder, getParameter(parameter), property);
	}

	@Override
	public String getApi(String urlBuilder, String parameter, Map<String, Object> property) throws Exception {

		logger.info("ApiServiceImpl getApi URL : " + urlBuilder);
		logger.info("ApiServiceImpl getApi PARAMETER : " + parameter);

		String restApiResponse = null;
		try {
			SSLConn.init();
			URL url = new URL(urlBuilder + "?" + parameter);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			setRequestProperty(conn, property);
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(1000 * 60);
			conn.setReadTimeout(1000 * 60);
			restApiResponse = restApiResponse(conn);
		} catch(Exception e) {
			logger.error("ApiServiceImpl getApi Exception : " + e.getLocalizedMessage());
		} finally {
		}

		return restApiResponse;
	}

	@Override
	public String postApi(String urlBuilder, Map<String, Object> parameter) throws Exception {
		return postApi(urlBuilder, getParameter(parameter), null);
	}

	@Override
	public String postApi(String urlBuilder, Map<String, Object> parameter, Map<String, Object> property) throws Exception {
		return postApi(urlBuilder, getParameter(parameter), property);
	}

	@Override
	public String postApi(String urlBuilder, String parameter, Map<String, Object> property) throws Exception {

		logger.info("ApiServiceImpl postApi URL : " + urlBuilder);
		logger.info("ApiServiceImpl postApi PARAMETER : " + parameter);

		String restApiResponse = null;
		
		try {
			
			SSLConn.init();
			URL url = new URL(urlBuilder);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			setRequestProperty(conn, property);
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(1000 * 60);
			conn.setReadTimeout(1000 * 300);
			conn.setDoOutput(true);
			conn.getOutputStream().write(parameter.getBytes("UTF-8"));
			restApiResponse = restApiResponse(conn);
			
		} catch(Exception e) {
			logger.error("ApiServiceImpl postApi Exception : " + e.getLocalizedMessage());
		} finally {
		}

		return restApiResponse;
		
	}
	
	@Override
	public String postApi(String urlBuilder, JsonObject parameter, Map<String, Object> property) throws Exception {
		
		logger.info("ApiServiceImpl postApi URL : " + urlBuilder);
		logger.info("ApiServiceImpl postApi PARAMETER : " + parameter);

		String restApiResponse = null;
		
		try {
			
			SSLConn.init();
			URL url = new URL(urlBuilder);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			setRequestProperty(conn, property);
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(1000 * 60);
			conn.setReadTimeout(1000 * 300);
			conn.setDoOutput(true);
			conn.getOutputStream().write(parameter.toString().getBytes("UTF-8"));
			restApiResponse = restApiResponse(conn);
			
		} catch(Exception e) {
			logger.error("ApiServiceImpl postApi Exception : " + e.getLocalizedMessage());
		} finally {
		}

		return restApiResponse;
		
	}

	public String getParameter(Map<String, Object> paramMap) throws Exception {
		
    	StringBuilder paramBuilder = new StringBuilder();
    	
    	for(Map.Entry<String, Object> param : paramMap.entrySet()) {
    		
    		if(param.getValue() != null){
    			
    			if(paramBuilder.length() != 0) {
    				paramBuilder.append("&");
    			}

    			paramBuilder.append(URLEncoder.encode(param.getKey(), "UTF-8"));
    			paramBuilder.append("=");
    			paramBuilder.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
    			
    		}
    		
    	}
    	
    	return paramBuilder.toString();

	}

	public void setRequestProperty(HttpURLConnection conn, Map<String, Object> property) {
		
		if (conn != null) {
			
			if (conn != null && property != null) {
				for (String key : property.keySet()) {
					
					if(!key.equals("content-type")) {
						conn.setRequestProperty(key, (String) property.get(key));
		        	}
					
				}
			} else {
				conn.setRequestProperty("Content-type", "application/json; charset=UTF-8");
			}
			
		}
		
	}

	public String restApiResponse(HttpURLConnection conn) throws Exception {

		String charsetName = "UTF-8";

		int responseCode = conn.getResponseCode();

		BufferedReader rd;
		
		if (responseCode >= 200 && responseCode <= 300) {
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), charsetName));
		} else {
			rd = new BufferedReader(new InputStreamReader(conn.getErrorStream(), charsetName));
		}

		StringBuilder sb = new StringBuilder();
		
		try {
			
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}
			
		} catch (IOException e) {
			logger.error("Api Call Error : " + conn.getURL());
			logger.error(e.getMessage());
		} finally {
			rd.close();
			conn.disconnect();
		}

		return sb.toString();
	}
	
}
