package com.manman.common.util;

import java.lang.reflect.Array;
import java.util.List;
import java.util.Map;

import jakarta.servlet.http.HttpServletRequest;

public class CommonUtil {
	
	//byte 단위로 문자열 잘라주는 함수
	public static String subStrByte(String str, int limitByte) {
		
		if(!str.isEmpty()) {
			str = str.trim();
			if(str.getBytes().length <= limitByte) {
				return str;
			}else {
				StringBuffer sbStr = new StringBuffer(limitByte);
				int nCnt = 0;
				
				for(char ch : str.toCharArray()) {
					nCnt += String.valueOf(ch).getBytes().length;
					if(nCnt > limitByte) {
						break;
					}
					sbStr.append(ch);
				}
				
				return sbStr.toString() + "...";
			}
		}else {
			return "";
		}
		
	}
	
	/**
     * Object type 변수가 비어있는지 체크
     * @param obj 
     * @return Boolean : true / false
     */
    public static Boolean empty(Object obj) {
        if (obj instanceof String) return obj == null || "".equals(obj.toString().trim());
        else if (obj instanceof List) return obj == null || ((List<?>) obj).isEmpty();
        else if (obj instanceof Map) return obj == null || ((Map<?, ?>) obj).isEmpty();
        else if (obj instanceof Object[]) return obj == null || Array.getLength(obj) == 0;
        else return obj == null;
    }
 
    /**
     * Object type 변수가 비어있지 않은지 체크
     * @param obj
     * @return Boolean : true / false
     */
    public static Boolean notEmpty(Object obj) {    	
        return !empty(obj);
    }
    
    // client ip
    public static String getClientIpAddr(HttpServletRequest request) {
    	
        String ip = request.getHeader("X-Forwarded-For");
     
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
     
        return ip;
    }

}
