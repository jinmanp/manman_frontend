package com.manman.common.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {
    
    private static final String[] WHITE_LIST = {
    		"/bootstrap/**"
    		, "/bootstrap-datepicker-1.6.4-dist/**"
    		, "/css/**"
    		, "/js/**"
    		, "/image/**"
    		, "/music/**"
    };
    
    @Bean
    public SecurityFilterChain config(HttpSecurity http) throws Exception {
    	
    	/*
    	http.csrf().disable();
        http.headers().frameOptions().disable();
        http.authorizeHttpRequests(authorize -> authorize
                        .requestMatchers(WHITE_LIST).permitAll());
        
        return http.build();
    	*/
    	
    	http.csrf().disable().authorizeHttpRequests()
	        // /about 요청에 대해서는 로그인을 요구함
	        .requestMatchers("/admin/**").authenticated()
	        // /admin 요청에 대해서는 ROLE_ADMIN 역할을 가지고 있어야 함
	        .requestMatchers("/admin/**").hasRole("ADMIN")
	        // 나머지 요청에 대해서는 로그인을 요구하지 않음
	        .anyRequest().permitAll()
        .and()
	        // 로그인하는 경우에 대해 설정함
	        .formLogin()
	        // 로그인 페이지를 제공하는 URL을 설정함
	        .loginPage("/admin/loginView")
	        .loginProcessingUrl("/admin/loginProc")
	        // 로그인 성공 URL을 설정함
	        .successForwardUrl("/admin/main")
	        // 로그인 실패 URL을 설정함
	        .failureHandler(new CustimAuthenticationFailureHandler())
	        .permitAll()
	    .and()
	        .logout()
	        .logoutRequestMatcher(new AntPathRequestMatcher("/admin/logoutProc"));

        return http.build();
        
    }

}