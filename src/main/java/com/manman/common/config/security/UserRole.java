package com.manman.common.config.security;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UserRole {
	
	USER("USER"),
    ADMIN("ADMIN");

    private String value;

}
