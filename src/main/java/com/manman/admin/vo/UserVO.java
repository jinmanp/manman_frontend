package com.manman.admin.vo;

import java.io.Serializable;

import com.manman.common.config.security.UserRole;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
public class UserVO implements Serializable {
	
	private static final long serialVersionUID = -4039055690054503888L;
	
	@Setter
	private String userId;
	
	@Setter
	private String userPw;
	
	@Setter
	private UserRole role;
	
	@Setter
    private Boolean isEnable = true; // 사용 여부
	
	@Builder
    public UserVO(String userId, String userPw){
        this.userId = userId;
        this.userPw = userPw;
    }
	
}
