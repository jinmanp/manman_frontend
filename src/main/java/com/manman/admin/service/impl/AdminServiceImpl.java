package com.manman.admin.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.manman.admin.service.AdminService;

@Service("AdminService")
@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
public class AdminServiceImpl implements AdminService {
	
	final private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
	
}
