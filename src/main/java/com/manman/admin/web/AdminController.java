package com.manman.admin.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.manman.admin.service.AdminService;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {
	
	final private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
    private AdminService adminService;

	@RequestMapping(value = "/loginView")
    public String loginView(){
        return "admin/login";
    }
    
	@RequestMapping("/")
    public String admin(Model model, @AuthenticationPrincipal User userInfo){
		
		model.addAttribute("userInfo", userInfo);

		return "admin/main";

	}
	
	@RequestMapping("/main")
    public String main(Model model, @AuthenticationPrincipal User userInfo){
		
		model.addAttribute("userInfo", userInfo);

		return "admin/main";

	}
	
}
