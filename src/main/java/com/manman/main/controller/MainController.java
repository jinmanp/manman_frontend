package com.manman.main.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.manman.main.vo.MusicVO;

@Controller
public class MainController {
	
	final private Logger logger = LoggerFactory.getLogger(this.getClass());
 
    @RequestMapping("/")
    public String index(Model model){
        return "index";
    }
    
    @RequestMapping({"/music", "/music/"})
    public String music(Model model, @ModelAttribute("musicVO") MusicVO musicVO){
    	
    	if(null == musicVO.getCategory() || "".equals(musicVO.getCategory())) {
    		musicVO.setCategory("R01");
    	}

		model.addAttribute("musicVO", musicVO);

		return "music/music";

	}
    
    @RequestMapping({"/personalDataProtection", "/personalDataProtection/"})
    public String personalDataProtection(Model model){
        return "personalDataProtection";
    }
    
}