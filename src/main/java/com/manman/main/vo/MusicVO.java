package com.manman.main.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MusicVO {
	
	private String regNo;
	private int rank;
	private String title;
	private String artist;
	private String regYmd;
	private String category;
	private String thumbnail; //youtube thumbnail url
	private String videoId; //youtube video key
	private String videoTitle; //youtube video title
	
}
