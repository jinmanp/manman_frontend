<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page import="org.springframework.security.core.Authentication" %>
<%
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	Object principal = auth.getPrincipal();
	
	String name = "";
	
	if(principal != null) {
		name = auth.getName();
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#ffffff">
	
    <link rel="shortcut icon" href="/image/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="60x60" href="/image/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/image/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/image/favicon-16x16.png">

	<title>Maandoo - 관리자 메인</title>
	
	<link rel="stylesheet" href="/bootstrap/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bootstrap/assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="/bootstrap/assets/css/Header-Dark.css">
    <link rel="stylesheet" href="/bootstrap/assets/css/Footer-Dark.css">
    
    <link rel="stylesheet" href="/css/style.css">

	<script src="/js/jquery-1.11.3.js"></script>
	<script src="/js/common.js"></script>
	
    <script src="/bootstrap/assets/bootstrap/js/bootstrap.min.js"></script>
	
	<script type="text/javascript">
	jQuery(document).ready(function(){

	});
	</script>

</head>
<body style="background-color:#212529;">
		
    <div class="header-dark" style="position: static;">
        <nav class="navbar navbar-dark navbar-expand-md navigation-clean-search">
            <div class="container">
            	<a class="navbar-brand" href="#">Maandoo</a>
            	<sec:authorize access="isAuthenticated()">
            		<form class="d-flex" method="post" action="/admin/logoutProc">
            			<h5><%=name %>님, 반갑습니다.</h5>
	            		<button class="btn btn-outline-success" type="submit">logout</button>
	            	</form>            	
            	</sec:authorize>
            </div>
        </nav>
    </div>
    
    <div class="container">
    	관리자 메인화면
    </div>
    
    <div class="footer-dark">
    	<footer>
    		<div class="container">
    			<p class="copyright">Contact : maandoo.com@gmail.com<br>Copyright ⓒ 2023 Maandoo. All Rights Reserved. </p>
    		</div>
    	</footer>
    </div>
    
</body>
</html>