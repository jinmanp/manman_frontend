<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="만두, 포털, 잡동사니, 개발공간, 분실물, 분실물 조회, lost112">
<meta name="description"
	content="최신 노래, 음악 듣기, 노래 듣기, 무료 노래, 무료 음악, TOP100 노래 듣기, 가수 노래듣기, Melon, 최신 가요, 유튜브, Youtube, 만두 뮤직, EDM, KPOP, POP, 최신가요 Hot 100 무료 연속듣기, 분실물, 분실물 조회, lost112">
<meta name="author" content="Maandoo.com">
<meta name="robots" content="index,follow">
<meta property="subject" content="Maandoo.com">
<meta itemprop="headline" content="Maandoo.com">
<meta itemprop="alternativeHeadline" content="Maandoo.com">
<meta itemprop="name" content="Maandoo.com">
<meta itemprop="description" content="Maandoo.com">

<meta property="og:locale" content="ko_KR">
<meta property="og:url" content="http://www.maandoo.com/">
<meta property="og:site_name" content="Maandoo.com">
<meta property="og:type" content="article">
<meta property="og:image"
	content="http://www.maandoo.com/image/maandoo.png">
<meta property="og:title" content="Maandoo.com - TOP 100 노래 듣기">
<meta property="og:description"
	content="최신 노래, 음악 듣기, 노래 듣기, 무료 노래, 무료 음악, TOP100 노래 듣기, 가수 노래듣기, Melon, 최신 가요, 유튜브, Youtube, 만두 뮤직, EDM, KPOP, POP, 최신가요 Hot 100 무료 연속듣기">
<meta property="og:keywords"
	content="최신 노래, 음악 듣기, 노래 듣기, 무료 노래, 무료 음악, TOP100 노래 듣기, 가수 노래듣기, Melon, 최신 가요, 유튜브, Youtube, 만두 뮤직, EDM, KPOP, POP, 최신가요 Hot 100 연속듣기">

<meta property="twitter:url" content="http://www.maandoo.com/">
<meta property="twitter:site" content="Maandoo.com">
<meta property="twitter:title" content="Maandoo.com - TOP 100 노래 듣기">
<meta property="twitter:image"
	content="http://www.maandoo.com/image/maandoo.png">
<meta property="twitter:description"
	content="최신 노래, 음악 듣기, 노래 듣기, 무료 노래, 무료 음악, TOP100 노래 듣기, 가수 노래듣기, Melon, 최신 가요, 유튜브, Youtube, 만두 뮤직, EDM, KPOP, POP, 최신가요 Hot 100 무료 연속듣기">

<meta property="nate:url" content="http://www.maandoo.com/">
<meta property="nate:site_name" content="Maandoo.com">
<meta property="nate:title" content="Maandoo.com - TOP 100 노래 듣기">
<meta property="nate:image"
	content="http://www.maandoo.com/image/maandoo.png">
<meta property="nate:description"
	content="최신 노래, 음악 듣기, 노래 듣기, 무료 노래, 무료 음악, TOP100 노래 듣기, 가수 노래듣기, Melon, 최신 가요, 유튜브, Youtube, 만두 뮤직, EDM, KPOP, POP, 최신가요 Hot 100 무료 연속듣기">

<meta itemprop="image"
	content="http://www.maandoo.com/image/maandoo.png">
<meta itemprop="url" content="http://www.maandoo.com/">
<meta itemprop="publisher" content="Maandoo.com - TOP 100 노래 듣기">
<meta itemprop="inLanguage" content="ko-kr">

<meta name="theme-color" content="#ffffff">

<link rel="canonical" href="http://www.maandoo.com/">
<link rel="alternate" href="http://www.maandoo.com/">

<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "WebSite",
		"url": "http://www.maandoo.com",
		"name": "Maandoo - 개발 프로젝트 모음",
		"author": {
			"@type": "Person",
			"name": "Maandoo"
		},
		"description": "최신 노래, 음악 듣기, 노래 듣기, 무료 노래, 무료 음악, TOP100 노래 듣기, 가수 노래듣기, Melon, 최신 가요, 유튜브, Youtube, 만두 뮤직, EDM, KPOP, POP, 최신가요 Hot 100 무료 연속듣기, 분실물, 분실물 조회, lost112, 경기지역화폐, 재난지원금",
		"sameAs" : [
			"http://www.maandoo.com/"
		]
	}
	</script>

<link rel="shortcut icon" href="/image/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" sizes="60x60"
	href="/image/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32"
	href="/image/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16"
	href="/image/favicon-16x16.png">

<title>Maandoo - 경력 소개 및 개인 포트폴리오</title>

<link rel="stylesheet"
	href="/bootstrap/assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="/bootstrap/assets/fonts/font-awesome.min.css">
<link rel="stylesheet" href="/bootstrap/assets/css/Header-Dark.css">
<link rel="stylesheet" href="/bootstrap/assets/css/Footer-Dark.css">

<link rel="stylesheet" href="/css/style.css">

<script src="/js/jquery-1.11.3.js"></script>
<script src="/js/common.js"></script>

<script src="/bootstrap/assets/bootstrap/js/bootstrap.min.js"></script>

<script async
	src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
	(adsbygoogle = window.adsbygoogle || []).push({
		google_ad_client : "ca-pub-4796638766531299",
		enable_page_level_ads : true
	});
</script>

<script type="text/javascript">
	jQuery(document).ready(function() {

		// .nav-link 클릭        
		$('.nav-link').click(function() {
			// active 해제
			$('.nav-link').removeClass('active');
			// 클릭한 위치 active 적용 
			$(this).addClass('active');

			$('.nav-contents').attr('class', 'nav-contents d-none');
			$('#' + $(this).text()).attr('class', 'nav-contents d-block');
		});
		
		// 운영체제 정보
	    var checkUserAgent = navigator.userAgent.toLowerCase();
		
	    if(checkUserAgent.indexOf('android') != -1) {	    	
	    	$('#smart_link').attr('href', 'https://play.google.com/store/apps/details?id=com.nsdis&hl=ko');
	    }else if(checkUserAgent.indexOf("iphone") != -1 || checkUserAgent.indexOf("ipad") != -1 || checkUserAgent.indexOf("ipod") != -1) {
	    	$('#smart_link').attr('href', 'https://apps.apple.com/kr/app/%EC%8A%A4%EB%A7%88%ED%8A%B8%EA%B5%AD%ED%86%A0%EC%A0%95%EB%B3%B4/id539398526');
	    }else {
	    	$('#smart_link').attr('href', 'https://play.google.com/store/apps/details?id=com.nsdis&hl=ko');
	    }

	});
</script>

</head>
<body style="background-color: #212529;">

	<div class="header-dark" style="position: static;">
		<nav
			class="navbar navbar-dark navbar-expand-md navigation-clean-search">
			<div class="container">
				<a class="navbar-brand" href="#">Maandoo</a>
			</div>
		</nav>
	</div>

	<div class="container">

		<div class="card mb-3">
			<div class="card-header"><strong>자기소개</strong></div>
			<div class="row no-gutters">
				<div class="col-md-3">
					<img class="card-img-top" src="/image/introduce.png" alt="introduce">
				</div>
				<div class="col-md-9">
					<div class="card-body">
						<h5 class="card-title"><strong>새로운 무언가를 만들어 가는 것이 즐거운 개발자</strong></h5>
						<p class="card-text">
							2024년 현재 14년차 개발자입니다.<br>
							분석, 설계, 개발(front-end, back-end, server), 배포까지 fullstack 개발자로 일하고 있습니다.<br>
							Spring Framework, Java, Jsp, DB(Oracle, PostgreSQL) 기술 위주 개발 경험이 있고 공간 정보(ArcGIS, PostGIS, GeoServer, OpenLayers) 관련 프로젝트 경험도 있습니다.<br>
							새로운 무언가를 만들어 서비스를 배포하고 관리하는게 재미있고 제 실력 향상에 도움이 된다고 느껴서 개인적으로 프로젝트를 진행한 경험이 있습니다.<br>
							<!-- 요즘 관심사는 Flutter 를 활용한 크로스 플랫폼 서비스 개발입니다. -->
						</p>
					</div>
				</div>
			</div>
		</div>

		<div class="card text-left">
			<div class="card-header">
				<ul class="nav nav-pills card-header-pills">
					<li class="nav-item"><a class="nav-link active" href="#">개발경력</a></li>
					<li class="nav-item"><a class="nav-link" href="#">개인프로젝트</a></li>
					<!-- <li class="nav-item"><a class="nav-link" href="#">기타</a></li> -->
				</ul>
			</div>
			
			<div id="개발경력" class="nav-contents d-block">
			
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_1_14_1.png" alt="index_1_14_1">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">
									<strong>정부24 생활맞춤형 서비스 구축 2단계 3차</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 정부24 원스톱서비스 개선<br>
									- 기술 스택 : 전자정부프레임워크, JBoss, Java, Jsp, Oracle, JavaScript, JQuery<br>
									- 업무 기간 : 2023.08 ~ 2024.04 (8개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 원스톱서비스 개선 팀 개발PL 역할 : 개발환경 분석, 개발가이드 배포 및 팀원 교육, 업무분석 및 설계, 기관 연계협의, 팀 개발 일정 관리<br>
										2. 개선 과업<br>
										 가족다문화지원 신규 서비스 구축<br>
										 전입신고+ 기능 개선<br>
										 원스톱서비스 업무처리창구 권한체계 개선<br>
										 원스톱서비스 처리상태 표준화
									</small>
								</p>
								<a href="https://www.gov.kr/portal/onestopSvc/transferReport" class="btn btn-primary" target="_blank">정부24 원스톱서비스 바로가기</a>
							</div>
						</div>
					</div>
				</div>
			
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_1_13_1.png" alt="index_1_13_1">
							<img class="card-img-top" src="/image/index_1_13_2.png" alt="index_1_13_2">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">
									<strong>클라우드 기반의 공간정보 데이터 통합 및 융복합 활용체계 구축 (3차)</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 시계열 데이터 구축, 시계열 데이터 변동 처리 모듈 개발<br>
									- 기술 스택 : PostgreSQL, PostGIS, Stored Procedure, Spring boot, Spring Batch, Java<br>
									- 업무 기간 : 2022.05 ~ 2023.02 (10개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 시계열 데이터구축 : 원천 데이터 분석 및 시계열 DB 설계, 원천 데이터를 활용한 시계열 데이터 구축 Procedure
										구현, 10여년간 축척된 1억여건의 대용량 공간 데이터(연속지적도, 개별지적도, 용도지역지구도 등) 시계열 초기 데이터로 구축(PostgreSQL, PostGIS, Stored Procedure)<br>
										2. 시계열 데이터 변동 처리 모듈 : 240여개 지자체에서 매일 변동되고 있는 변동 분 데이터를 시계열 데이터에 적용하는 변동 처리 모듈 개발(Spring boot, Spring Batch, Java)
									</small>
								</p>
								<h5 class="card-title">
									<strong>클라우드 기반의 공간정보 데이터 통합 및 융복합 활용체계 구축 (2차)</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 지적전산자료 제공 API 개발, 대용량 데이터추출 모듈 개발<br>
									- 기술 스택 : Docker, Spring boot, Swagger, Spring Batch, Quartz Job Scheduler, Java, PostgreSQL<br>
									- 업무 기간 : 2021.05 ~ 2022.03 (11개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 지적전산자료 제공 API : 지적전산자료 제공 API 기능을 Spring boot, Swagger 적용하여 개발, Docker 컨테이너(MSA)에서 서비스(Docker, Spring boot, Swagger)<br>
										2. 지적전산자료 추출 모듈 : Oracle, ProC 로 운영되고 있던 legacy 기능을 PostgreSQL, Spring Batch 로 전환하여 성능 개선(Spring Batch, Quartz Job Scheduler, Java, PostgreSQL)
									</small>
								</p>
								<h5 class="card-title">
									<strong>클라우드 기반의 공간정보 데이터 통합 및 융복합 활용체계 구축 (1차)</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 대용량 데이터 추출모듈 개발, 데이터 추출 관리자 기능 개발<br>
									- 기술 스택 : Spring Batch, Quartz Job Scheduler, Java, PostgreSQL, Spring Framework, jboss, Jsp<br>
									- 업무 기간 : 2020.05 ~ 2021.02 (10개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 대용량 데이터 추출 모듈 : 지정된 스케줄에 주기적으로 대용량 데이터 파일 추출, 사용자가 요청한 즉시 데이터 파일 추출할 수 있는 기능 구현(Spring Batch, Quartz Job Scheduler, Java, PostgreSQL)<br>
										2. 데이터 추출 관리자 기능 : 관리자 화면에서 추출할 데이터 선택, 데이터 추출 범위, 데이터 추출 스케줄 등을 설정할 수 있는 기능. 추출 상태 모니터링, 통계 정보 조회 등 기능 구현(Spring Framework, jboss, Jsp)
									</small>
								</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_1_12_1.png" alt="index_1_12_1">
							<img class="card-img-top" src="/image/index_1_12_2.png" alt="index_1_12_2">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">
									<strong>행정서비스 통합제공시스템(정부24) 구축 (2단계 3차)</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 온종일돌봄 서비스 개발<br>
									- 기술 스택 : Spring Framework, jboss, Java, Jsp, Oracle<br>
									- 업무 기간 : 2019.05 ~ 2020.05 (13개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 정부24 신규 서비스(온종일돌봄) 구축 : 초등학생 대상 돌봄 서비스 정보를 한 곳에서 조회하고 신청 할 수 있는 서비스<br>
										2. 대국민 신청 기능 개발(Spring Framework, jboss, Java, Jsp, Oracle)<br>
										3. 돌봄시설 업무용 관리자 기능 개발(Spring Framework, jboss, Java, Jsp, Oracle)<br>
									</small>
								</p>
								<a href="https://www.gov.kr/portal/onestopSvc/alldaycare" class="btn btn-primary" target="_blank">정부24 바로가기</a>
							</div>
						</div>
					</div>
				</div>
				
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_1_11_1.png" alt="index_1_11_1">
							<img class="card-img-top" src="/image/index_1_11_2.png" alt="index_1_11_2">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">
									<strong>경기부동산포털 유지보수 사업</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 서비스 운영 지원<br>
									- 기술 스택 : 전자정부프레임워크, Jeus, Java, Jsp, ArcGIS<br>
									- 업무 기간 : 2019.01 ~ 2019.04 (4개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 신규 오픈 서비스 안정화 지원(전자정부프레임워크, Jeus, Java, Jsp, ArcGIS)<br>
									</small>
								</p>
								<h5 class="card-title">
									<strong>경기도 부동산포털 웹서비스 환경개선 사업</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 프로젝트 매니져 (PM)<br>
									- 기술 스택 : 전자정부프레임워크, Jeus, Java, Jsp, ArcGIS<br>
									- 업무 기간 : 2018.05 ~ 2018.12 (8개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 제안서 작성 및 제안발표<br>
										2. 프로젝트 수주 및 사업장 세팅<br>
										3. 설계 산출물 테일러링<br>
										4. 프로젝트 일정 관리<br>
										5. 주간보고, 월간보고, 착수보고회, 중간보고회, 완료보고회, 자문회의 등 진행<br>
										6. 개발 서포트(전자정부프레임워크, Jeus, Java, Jsp, ArcGIS)<br>
									</small>
								</p>
								<a href="https://gris.gg.go.kr" class="btn btn-primary" target="_blank">경기부동산포털 바로가기</a>
							</div>
						</div>
					</div>
				</div>
				
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_1_10_1.png" alt="index_1_10_1">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">
									<strong>클라우드기반 국가공간정보시스템 및 데이터관리체계 개편 연구</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 시계열 공간 데이터 구축<br>
									- 기술 스택 : Oracle, ArcGIS<br>
									- 업무 기간 : 2018.03 ~ 2018.04 (2개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 지적도 이력 데이터를 활용하여 과거 특정 시점의 지적도 정보를 조회할 수 있는 시계열 데이터 구축(Oracle, ArcGIS)<br>
										2. 시계열 데이터를 조회할 수 있는 웹 서비스 프로토타입 구현(ArcGIS Javascript API)<br>
									</small>
								</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_1_9_1.png" alt="index_1_9_1">
							<img class="card-img-top" src="/image/index_1_9_2.png" alt="index_1_9_2">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">
									<strong>한국토지정보시스템 고도화 사업</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 시스템 모니터링 기능 개발, 엑셀 작성 내용 유효성 검증 기능 개발<br>
									- 기술 스택 : Unix, Java, Jsp, Shell Script, Excel Visual Basic<br>
									- 업무 기간 : 2017.06 ~ 2018.02 (9개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 시스템 모니터링 기능 개발 : 운영 중인 서버들의 CPU, Memory, Disk 용량 등을 모니터링, 운영 중인 서비스 상태 모니터링, 서비스 재기동 등(Unix, Java, Jsp, Shell Script)<br>
										2. 엑셀 유효성 검증 : Excel Visual Basic 을 활용하여 엑셀 작성 내용이 업무 규칙에 맞게 입력되어 있는지 체크하는 기능 개발(Excel Visual Basic)<br>
									</small>
								</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_1_8_1.png" alt="index_1_8_1">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">
									<strong>국가공간정보통합체계 고도화 사업</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 지오코딩 기능 개발<br>
									- 기술 스택 : Java, Oracle<br>
									- 업무 기간 : 2017.01 ~ 2017.02 (2개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 지번주소, 새주소 텍스트를 분석하여 필지고유번호(PNU) 및 위경도 좌표로 변환하는 기능 개발(Java, Oracle)<br>
									</small>
								</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_1_7_1.png" alt="index_1_7_1">
							<img class="card-img-top" src="/image/index_1_7_2.png" alt="index_1_7_2">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">
									<strong>국토교통부 북한지적원도 정보화 사업</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 북한지적원도 시스템 개발<br>
									- 기술 스택 : 전자정부프레임워크, Jeus, Oracle, ArcGIS Server, Java, Jsp, ArcGIS Javascript API<br>
									- 업무 기간 : 2016.01 ~ 2016.12 (12개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 북한지적원도 활용 시스템 개발(시스템 개발 총괄)<br>
										2. 전자정부프레임워크(Spring 기반) 세팅 및 공통 기능 개발<br>
										3. 윈도우 기반 개발서버 세팅(전자정부프레임워크, Jeus, Oracle)<br>
										4. ArcGIS 기반 지도서버 세팅(ArcGIS Server)<br>
										5. 북한지역 지적원도 조회 시스템 구축(Java, Jsp, ArcGIS Javascript API)
									</small>
								</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_1_6_1.png" alt="index_1_6_1">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">
									<strong>국토교통부 부동산종합정보 품질개선 및 개방DB 구축 사업</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 부동산종합정보 자료정비<br>
									- 기술 스택 : Java, Oracle, Oracle Procedure<br>
									- 업무 기간 : 2015.06 ~ 2015.12 (7개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 부동산종합정보 데이터 민간 개방 시 품질 문제를 개선하기 위한 오류 데이터 검증 및 개선 작업(Java, Oracle, Oracle Procedure)
									</small>
								</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_1_5_1.png" alt="index_1_5_1">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">
									<strong>국토교통부 국토정보시스템 개인정보암호화 사업</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : ETL프로그램 개인정보 암호화<br>
									- 기술 스택 : DataStage, Oracle<br>
									- 업무 기간 : 2015.01 ~ 2015.05 (5개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 국토정보시스템이 보유중인 개인정보 암호화 작업(Oracle)<br>
										2. ETL 프로그램 소스 수정 및 데이터 검증(DataStage)
									</small>
								</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_1_4_1.png" alt="index_1_4_1">
							<img class="card-img-top" src="/image/index_1_4_2.png" alt="index_1_4_2">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">
									<strong>국토교통부 국토정보시스템 기능고도화 사업</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 공공보상알리미 웹서비스 개발<br>
									- 기술 스택 : 전자정부프레임워크, Jeus, Oracle, ArcGIS Server, Java, Jsp, ArcGIS Javascript API<br>
									- 업무 기간 : 2014.01 ~ 2014.12 (12개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 공공개발 사업으로 인한 개인이 소유한 토지 및 건축물의 보상 정보를 확인 할 수 있는 대국민 서비스 개발<br>
										2. 전자정부프레임워크 세팅 및 공통 기능 개발<br>
										3. 리눅스 기반 개발서버 세팅(CentOS, Jeus)<br>
										4. 공공보상알리미 지도기반 정보조회 서비스 개발(Oracle, ArcGIS Server, Java, Jsp, ArcGIS Javascript API)<br>
										5. 회원가입, 로그인 기능 개발<br>
										6. 공공아이핀, 공인인증서 API 적용 기능 개발 
									</small>
								</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_1_3_1.png" alt="index_1_3_1">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">
									<strong>국토교통부 국토정보시스템 기능고도화 사업</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 스마트국토정보 APP 개발, 공공보상현장지원시스템 APP 개발<br>
									- 기술 스택 : Android, IOS, jQuery Mobile API<br>
									- 업무 기간 : 2013.01 ~ 2013.12 (12개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 스마트국토정보(Android, IOS, MobileWeb) 앱 기능고도화, 공공보상현장지원(Android) 앱 기능고도화<br>
										2. 스마트국토정보 실거래가 서비스, 토지이용계획 서비스 개발(Java 서버 개발, 운영체제 별 클라이언트 개발)<br>
										3. 공공보상현장지원 지도서비스 개발 및 속성정보 조회 및 입력, 수정 기능 개발(Java 서버 개발. Android 클라이언트 개발)<br>
										4. APP 배포 및 버전 관리, 사용자 민원 응대 및 사용 통계 관리
									</small>
								</p>
								<a id="smart_link" href="https://play.google.com/store/apps/details?id=com.nsdis&hl=ko" class="btn btn-primary" target="_blank">스마트국토정보 바로가기</a>
							</div>
						</div>
					</div>
				</div>
				
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_1_2_1.png" alt="index_1_2_1">
							<img class="card-img-top" src="/image/index_1_2_2.png" alt="index_1_2_2">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">
									<strong>국토해양부 국토정보시스템 기능고도화 사업</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 국토정보 공동활용체계 OpenAPI 서비스 개발<br>
									- 기술 스택 : anyframe, Jeus, Oracle, ArcGIS Server, Java, Jsp, ArcGIS Javascript API<br>
									- 업무 기간 : 2012.04 ~ 2012.12 (9개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 국토정보시스템에서 보유중인 토지, 건축물, 연속지적도, 용도지역지구도 OpenAPI 서비스 개발<br>
										2. 지도 서비스 API (ArcGIS Map Service) 및 속성 서비스 API (Restful Service)개발<br>
										3. API 사용신청 및 승인, 권한관리 기능 개발<br>
										4. 운영서버 배포 및 테스트 작업
									</small>
								</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_1_1_1.png" alt="index_1_1_1">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title">
									<strong>국토해양부 국가공간정보센터 유지보수 사업</strong>
								</h5>
								<p class="card-text">
									- 참여 업무 : 지적도면통합시스템 유지보수<br>
									- 기술 스택 : Unix, Oracle, ArcSDE<br>
									- 업무 기간 : 2011.04 ~ 2012.03 (12개월)<br>
									- 상세 내용<br>
									<small class="text-muted">
										1. 230여개 시군구에서 만들어지는 개별지적도를 전국단위로 취합하는 시스템 관리(Unix, Oracle, ArcSDE)<br>
										2. 민원응대 및 처리<br>
										3. 개별지적도 취합 현황 및 통계 관리
									</small>
								</p>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			
			<div id="개인프로젝트" class="nav-contents d-none">
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_2_1_1.png"
								alt="경기지역화폐 가맹점 찾기">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title"><strong>경기지역화폐 가맹점 찾기</strong></h5>
								<p class="card-text">경기지역화폐 가맹점 찾기는 경기도 내 지역화폐를 사용할 수 있는 점포를
									쉽고 빠르게 검색하고 전화번호와 위치를 확인할 수 있는 서비스입니다.</p>
								<p class="card-text">
									주요기능 <small class="text-muted"><br> 1. 통합검색 : 상점명,
										상점종류, 도로명주소, 지번주소<br> 2. 내 위치 주변 가까운 거리 순 사용처 검색<br>
										3. 길찾기 기능 제공(네비게이션 앱 연결)<br> 4. 지도 화면을 통한 가맹점 위치 정보 제공</small>
								</p>
								<p class="card-text">
									적용기술 <small class="text-muted"><br>java, android,
										firebase, retrofit, rxjava, 네이버 지도 SDK, sqlite, room,
										spatia-room</small>
								</p>
								<p class="card-text">
									git
									<small class="text-muted">
										<br><a href="https://gitlab.com/jinmanp/ggmoney.git" target="_blank">https://gitlab.com/jinmanp/ggmoney.git</a>
									</small>
								</p>								
								<a
									href="https://play.google.com/store/apps/details?id=com.manman.ggmoney"
									class="btn btn-primary" target="_blank">googlePlay 바로가기</a>
							</div>
						</div>
					</div>
				</div>
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_2_1_2.png"
								alt="분실물창고">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title"><strong>분실물창고 - 잃어버린 물건 찾기</strong></h5>
								<p class="card-text">경찰서 및 운수회사에서 보관 중인 습득물 조회 및 경찰서에 신고된
									분실물을 조회 할 수 있는 서비스입니다.</p>
								<p class="card-text">
									주요기능 <small class="text-muted"><br> 1. 습득물 조회 :
										경찰서 및 운수회사에서 보관 중인 습득물을 조회 할 수 있습니다. <br>2. 분실물 조회 : 경찰서에
										신고된 분실물을 조회 할 수 있습니다.<br> 3. 상세 검색 : 기간, 물품 분류, 분실 지역,
										분실물 이름 등의 조건을 설정하여 검색이 가능합니다.<br> 4. 알림 설정 : 찾고싶은 분실물 정보를
										설정하면 해당 조건의 분실물 정보가 새로 등록되었을 때 또는 분실물 상태가 변경되었을 때 휴대폰으로 알림이
										발송됩니다.</small>
								</p>
								<p class="card-text">
									적용기술 <small class="text-muted"><br>1. front-end<br>안드로이드 앱 : java,
										android, firebase-messaging, glide<br> 2. back-end<br>
										데이터 갱신 모듈 : AWS Lightsail, Spring Boot, Spring Batch, Quartz
										Job Scheduler, Java, PostgreSQL<br> API 서비스 : AWS
										Lightsail, Spring Boot, Swagger, java, mybatis, PostgreSQL</small>
								</p>
								<p class="card-text">
									git
									<small class="text-muted">
										<br>안드로이드 앱 : <a href="https://gitlab.com/jinmanp/lostarticle.git" target="_blank">https://gitlab.com/jinmanp/lostarticle.git</a>
										<br>API 서비스 : <a href="https://gitlab.com/jinmanp/manman_swagger.git" target="_blank">https://gitlab.com/jinmanp/manman_swagger.git</a>
										<br>데이터 갱신 모듈 : <a href="https://gitlab.com/jinmanp/quartz_scheduler.git" target="_blank">https://gitlab.com/jinmanp/quartz_scheduler.git</a>
									</small>
								</p>	
								<a
									href="https://play.google.com/store/apps/details?id=com.manman.lostarticle"
									class="btn btn-primary" target="_blank">googlePlay 바로가기</a>
							</div>
						</div>
					</div>
				</div>
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_2_1_3.png" alt="만두뮤직">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title"><strong>MD Music - TOP 100 노래 듣기</strong></h5>
								<p class="card-text">실시간 인기가요를 뮤직비디오와 함께 무료로 감상할 수 있는 서비스</p>
								<p class="card-text">
									주요기능 <small class="text-muted"><br>1. 검색 : 원하는 음악을
										검색하여 마이페이지에 저장 하실 수 있습니다.<br> 2. 마이페이지 : 저장하신 음악을 마이페이지에서
										감상 하실 수 있습니다.<br> 3. 장르별 감상 : 실시간 TOP 100 인기가요, KPOP, 해외
										POP, 발라드, 힙합, R&B, 일렉트로닉, Rock. 재즈, 트로트, OST, 국악, CCM, 동요, 년도별
										인기가요 등 장르 별 음악을 선택하여 감상하실 수 있습니다.<br>4. 가사보기 : 음악을 감상하시면서
										노래 가사를 같이 확인 하실 수 있습니다.</small>
								</p>
								<p class="card-text">
									적용기술 <small class="text-muted"><br>1. front-end<br>
										AWS Lightsail, SSL(Let’s Encrypt 적용), Tomcat, Spring Boot,
										java, jsp, jQuery, Bootstrap<br> 2. back-end<br> 데이터
										크롤링 및 갱신 모듈 : AWS Lightsail, youtube v3 API, jsoup, Spring
										Boot, Spring Batch, Quartz Job Scheduler, Java, PostgreSQL<br>
										API 서비스 : AWS Lightsail, Spring Boot, Swagger, java, mybatis,
										PostgreSQL</small>
								</p>
								<p class="card-text">
									git
									<small class="text-muted">
										<br>front-end 서비스 : <a href="https://gitlab.com/jinmanp/manman_frontend.git" target="_blank">https://gitlab.com/jinmanp/manman_frontend.git</a>
										<br>API 서비스 : <a href="https://gitlab.com/jinmanp/manman_swagger.git" target="_blank">https://gitlab.com/jinmanp/manman_swagger.git</a>
										<br>데이터 갱신 모듈 : <a href="https://gitlab.com/jinmanp/quartz_scheduler.git" target="_blank">https://gitlab.com/jinmanp/quartz_scheduler.git</a>
									</small>
								</p>
								<a href="https://www.maandoo.com/music" class="btn btn-primary"
									target="_blank">Go MD Music</a>
							</div>
						</div>
					</div>
				</div>
				<div class="card m-3">
					<div class="row no-gutters">
						<div class="col-md-3">
							<img class="card-img-top" src="/image/index_2_1_4.png" alt="블로그">
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<h5 class="card-title"><strong>개발 블로그</strong></h5>
								<p class="card-text">개발하면서 기억해야 할 내용 기록
									<small class="text-muted"><br>
										PostGIS, PostgresSQL, Spring, Servers, Java, ArcGIS, Android, iOS, Web 등
									</small>
								</p>
								<a href="https://jinmanp.tistory.com/" class="btn btn-primary"
									target="_blank">블로그 바로가기</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- 
			<div id="기타" class="nav-contents d-none">
				기타_contents
			</div>
			 -->
		</div>
		<!-- 
		<div class="row">
			<div class="col-md-12">
				<div>
					반응형 2
					<ins class="adsbygoogle"
						 style="display:block"
						 data-ad-client="ca-pub-4796638766531299"
						 data-ad-slot="1362907193"
						 data-ad-format="auto"
						 data-full-width-responsive="true"></ins>
					<script>
						 (adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
			</div>
		</div>
 	-->
	</div>
	<div class="footer-dark">
		<footer>
			<div class="container">
				<p class="copyright">
					Contact : maandoo.com@gmail.com<br>Copyright ⓒ 2023 Maandoo. All Rights Reserved.
				</p>
			</div>
		</footer>
	</div>

</body>
</html>