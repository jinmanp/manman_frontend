<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="shortcut icon" href="/image/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="60x60" href="/image/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/image/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/image/favicon-16x16.png">

	<title>개인정보처리방침</title>
	
	<link rel="stylesheet" href="/bootstrap/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bootstrap/assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="/bootstrap/assets/css/Header-Dark.css">
    <link rel="stylesheet" href="/bootstrap/assets/css/Footer-Dark.css">
    
    <link rel="stylesheet" href="/css/style.css">

	<script src="/js/jquery-1.11.3.js"></script>
	<script src="/js/common.js"></script>
	
    <script src="/bootstrap/assets/bootstrap/js/bootstrap.min.js"></script>
	
	<script type="text/javascript">
	jQuery(document).ready(function(){

	});
	</script>

</head>
<body style="background-color:#212529;">
		
    <div class="header-dark" style="position: static;">
        <nav class="navbar navbar-dark navbar-expand-md navigation-clean-search">
            <div class="container"><a class="navbar-brand" href="#">Maandoo</a></div>
        </nav>
    </div>
    
    <div class="container">
    	<div class="page-header">
		  <h3 class="text-white">1. 개인정보의 처리 목적</h3>
		</div>
		<p class="text-white">
			maandoo는 다음의 목적을 위하여 개인정보를 처리하고 있으며, 다음의 목적 이외의 용도로는 이용하지 않습니다.<br><br>
			- 고객 가입의사 확인, 고객에 대한 서비스 제공에 따른 본인 식별.인증, 회원자격 유지.관리, 물품 또는 서비스 공급에 따른 금액 결제, 물품 또는 서비스의 공급.배송 등<br>
		</p>
		<div class="page-header">
		  <h3 class="text-white">2. 개인정보의 처리 및 보유 기간</h3>
		</div>
		<p class="text-white">maandoo는 정보주체로부터 개인정보를 수집할 때 동의 받은 개인정보 보유, 이용기간 또는 법령에 따른 개인정보 보유․이용기간 내에서 개인정보를 처리․보유합니다.<br></p>
		<div class="page-header">
		  <h3 class="text-white">3. 정보주체와 법정대리인의 권리·의무 및 그 행사방법</h3>
		</div>
		<p class="text-white">
			정보주체는 maandoo에 대해 언제든지 다음 각 호의 개인정보 보호 관련 권리를 행사할 수 있습니다.<br><br>
			1. 개인정보 열람요구<br>
			2. 오류 등이 있을 경우 정정 요구<br>
			3. 삭제요구<br>
			4. 처리정지 요구<br>
		</p>
		<div class="page-header">
		  <h3 class="text-white">4. 처리하는 개인정보의 항목</h3>
		</div>
		<p class="text-white">
			maandoo는 다음의 개인정보 항목을 처리하고 있습니다.<br><br>
			- 없음<br>
		</p>
		<div class="page-header">
		  <h3 class="text-white">5. 개인정보의 파기</h3>
		</div>
		<p class="text-white">
			maandoo는 원칙적으로 개인정보 처리목적이 달성된 경우에는 지체없이 해당 개인정보를 파기합니다. 파기의 절차, 기한 및 방법은 다음과 같습니다.<br><br>
			-파기절차<br>
			이용자가 입력한 정보는 목적 달성 후 별도의 DB에 옮겨져(종이의 경우 별도의 서류) 내부 방침 및 기타 관련 법령에 따라 일정기간 저장된 후 혹은 즉시 파기됩니다. 이 때, DB로 옮겨진 개인정보는 법률에 의한 경우가 아니고서는 다른 목적으로 이용되지 않습니다.<br><br>
			-파기기한<br>
			이용자의 개인정보는 개인정보의 보유기간이 경과된 경우에는 보유기간의 종료일로부터 5일 이내에, 개인정보의 처리 목적 달성, 해당 서비스의 폐지, 사업의 종료 등 그 개인정보가 불필요하게 되었을 때에는 개인정보의 처리가 불필요한 것으로 인정되는 날로부터 5일 이내에 그 개인정보를 파기합니다.<br>
		</p>
		<div class="page-header">
		  <h3 class="text-white">6. 개인정보 자동 수집 장치의 설치 운영 및 거부에 관한 사항</h3>
		</div>
		<p class="text-white">
			maandoo는 정보주체의 이용정보를 저장하고 수시로 불러오는 ‘쿠키’를 사용하지 않습니다.<br>
		</p>
		<div class="page-header">
		  <h3 class="text-white">7. 개인정보 보호책임자</h3>
		</div>
		<p class="text-white">
			maandoo는 개인정보 처리에 관한 업무를 총괄해서 책임지고, 개인정보 처리와 관련한 정보주체의 불만처리 및 피해구제 등을 위하여 아래와 같이 개인정보 보호책임자를 지정하고 있습니다.<br><br>
			- 개인정보 보호책임자<br>
			성명 : 박진만<br>
			연락처 : pjm1773@gmail.com<br><br>
			정보주체께서는 maandoo의 서비스을 이용하시면서 발생한 모든 개인정보 보호 관련 문의, 불만처리, 피해구제 등에 관한 사항을 개인정보 보호책임자 및 담당부서로 문의하실 수 있습니다.<br>
			maandoo는 정보주체의 문의에 대해 지체 없이 답변 및 처리해드릴 것입니다.<br>
		</p>
		<div class="page-header">
		  <h3 class="text-white">8. 개인정보 처리방침 변경</h3>
		</div>
		<p class="text-white">
			이 개인정보처리방침은 시행일로부터 적용되며, 법령 및 방침에 따른 변경내용의 추가, 삭제 및 정정이 있는 경우에는 변경사항의 시행 7일 전부터 공지사항을 통하여 고지할 것입니다.<br>
		</p>
		<div class="page-header">
		  <h3 class="text-white">9. 개인정보의 안전성 확보 조치</h3>
		</div>
		<p class="text-white">
			maandoo는 개인정보보호법 제29조에 따라 다음과 같이 안전성 확보에 필요한 기술적/관리적 및 물리적 조치를 하고 있습니다.<br><br>
			- 개인정보 취급 직원의 최소화 및 교육<br>
			개인정보를 취급하는 직원을 지정하고 담당자에 한정시켜 최소화 하여 개인정보를 관리하는 대책을 시행하고 있습니다.<br>
		</p>
    </div>
    
    <div class="footer-dark">
    	<footer>
    		<div class="container">
    			<p class="copyright">Contact : maandoo.com@gmail.com<br>Copyright ⓒ 2023 Maandoo. All Rights Reserved. </p>
    		</div>
    	</footer>
    </div>
    
</body>
</html>