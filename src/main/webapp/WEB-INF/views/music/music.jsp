<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
 /**
  * @Class Name : music.jsp
  * @Description : 
  * @Modification Information
  * @
  * @ 수정일      		수정자            수정내용
  * @ -------       --------    ---------------------------
  * @ 2018.04.16   	박진만          	최초 생성
  *
  *  @author jinmanp
  *  @since 2018.04.16
  *  @version 1.0
  *  @see
  */
%>
<!DOCTYPE html>
<html>

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118068994-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		
		gtag('config', 'UA-118068994-2');
	</script>
	
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="최신 노래, 음악 듣기, 노래 듣기, 무료 노래, 무료 음악, TOP100 노래 듣기, 가수 노래듣기, Melon, 최신 가요, 유튜브, Youtube, 만두 뮤직, EDM, KPOP, POP, 최신가요 Hot 100 연속듣기">
    <meta name="description" content="최신 노래, 음악 듣기, 노래 듣기, 무료 노래, 무료 음악, TOP100 노래 듣기, 가수 노래듣기, Melon, 최신 가요, 유튜브, Youtube, 만두 뮤직, EDM, KPOP, POP, 최신가요 Hot 100 무료 연속듣기">
    <meta name="author" content="Maandoo Music">
    <meta name="robots" content="index,follow">
    <meta property="subject" content="K-Pop Hot 100 Chart - 최신 가요 Top 100 연속 듣기 - Maandoo Music">
    <meta itemprop="headline" content="K-Pop Hot 100 Chart - 최신 가요 Top 100 연속 듣기 - Maandoo Music">
    <meta itemprop="alternativeHeadline" content="K-Pop Hot 100 Chart - 최신 가요 Top 100 연속 듣기 - Maandoo Music">
    <meta itemprop="name" content="K-Pop Hot 100 Chart - 최신 가요 Top 100 연속 듣기 - Maandoo Music">
    <meta itemprop="description" content="최신 노래, 음악 듣기, 노래 듣기, 무료 노래, 무료 음악, TOP100 노래 듣기, 가수 노래듣기, Melon, 최신 가요, 유튜브, Youtube, 만두 뮤직, EDM, KPOP, POP, 최신가요 Hot 100 무료 연속듣기">
    
    <meta property="fb:app_id" content="241123743301022">
    <meta property="og:locale" content="ko_KR">
    <meta property="og:url" content="http://www.maandoo.com/music/">
    <meta property="og:site_name" content="Maandoo Music">
    <meta property="og:type" content="article">
    <meta property="og:image" content="http://www.maandoo.com/image/maandoo.png">
    <meta property="og:title" content="K-Pop Hot 100 Chart - 최신 가요 Top 100 연속 듣기 - Maandoo Music">
    <meta property="og:description" content="최신 노래, 음악 듣기, 노래 듣기, 무료 노래, 무료 음악, TOP100 노래 듣기, 가수 노래듣기, Melon, 최신 가요, 유튜브, Youtube, 만두 뮤직, EDM, KPOP, POP, 최신가요 Hot 100 무료 연속듣기">
    <meta property="og:keywords" content="최신 노래, 음악 듣기, 노래 듣기, 무료 노래, 무료 음악, TOP100 노래 듣기, 가수 노래듣기, Melon, 최신 가요, 유튜브, Youtube, 만두 뮤직, EDM, KPOP, POP, 최신가요 Hot 100 연속듣기">
    
    <meta property="twitter:url" content="http://www.maandoo.com/music/">
    <meta property="twitter:site" content="Maandoo Music">
    <meta property="twitter:title" content="K-Pop Hot 100 Chart - 최신 가요 Top 100 연속 듣기 - Maandoo Music">
    <meta property="twitter:image" content="http://www.maandoo.com/image/maandoo.png">
    <meta property="twitter:description" content="최신 노래, 음악 듣기, 노래 듣기, 무료 노래, 무료 음악, TOP100 노래 듣기, 가수 노래듣기, Melon, 최신 가요, 유튜브, Youtube, 만두 뮤직, EDM, KPOP, POP, 최신가요 Hot 100 무료 연속듣기">
    
    <meta property="nate:url" content="http://www.maandoo.com/music/">
    <meta property="nate:site_name" content="Maandoo Music">
    <meta property="nate:title" content="K-Pop Hot 100 Chart - 최신 가요 Top 100 연속 듣기 - Maandoo Music">
    <meta property="nate:image" content="http://www.maandoo.com/image/maandoo.png">
    <meta property="nate:description" content="최신 노래, 음악 듣기, 노래 듣기, 무료 노래, 무료 음악, TOP100 노래 듣기, 가수 노래듣기, Melon, 최신 가요, 유튜브, Youtube, 만두 뮤직, EDM, KPOP, POP, 최신가요 Hot 100 무료 연속듣기">
    
    <meta itemprop="image" content="http://www.maandoo.com/image/maandoo.png">
    <meta itemprop="url" content="http://www.maandoo.com/music/">
    <meta itemprop="publisher" content="K-Pop Hot 100 Chart - 최신 가요 Top 100 연속 듣기 - Maandoo Music">
    <meta itemprop="inLanguage" content="ko-kr">    
    
    <meta name="theme-color" content="#ffffff">
    
    <link rel="canonical" href="http://www.maandoo.com/music/">
    <link rel="alternate" href="http://www.maandoo.com/music/">
    
    <script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "WebSite",
		"url": "http://www.maandoo.com/music/",
		"name": "K-Pop Hot 100 Chart - 최신 가요 Top 100 연속 듣기 - Maandoo Music",
		"author": {
			"@type": "Person",
			"name": "Maandoo"
		},
		"description": "최신 노래, 음악 듣기, 노래 듣기, 무료 노래, 무료 음악, TOP100 노래 듣기, 가수 노래듣기, Melon, 최신 가요, 유튜브, Youtube, 만두 뮤직, EDM, KPOP, POP, 최신가요 Hot 100 무료 연속듣기",
		"sameAs" : [
			"http://www.maandoo.com/"
		]
	}
	</script>
	
    <link rel="shortcut icon" href="/image/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="60x60" href="/image/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/image/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/image/favicon-16x16.png">
    
    <title>K-Pop Hot 100 Chart - 최신 가요 Top 100 연속 듣기 - Maandoo Music</title>
    
    <link rel="stylesheet" href="/bootstrap/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bootstrap/assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bitter:400,700">
    <link rel="stylesheet" href="/bootstrap/assets/css/Header-Dark.css">
    <link rel="stylesheet" href="/bootstrap/assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="/bootstrap-datepicker-1.6.4-dist/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/css/jquery-ui.min.css">
    <link rel="stylesheet" href="/css/style.css">

	<script src="/js/jquery-1.11.3.js"></script>
	<script src="/js/jquery-ui.min.js"></script>
	<script src="/js/common.js"></script>
	<script src="/js/jwt-decode.js"></script>
	
    <script src="/bootstrap/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.min.js"></script>
    <script src="/bootstrap-datepicker-1.6.4-dist/locales/bootstrap-datepicker.kr.min.js"></script>
    
    <script data-ad-client="ca-pub-4796638766531299" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	
	<script>
	
	var currentVideoId = "";
	var currentIndex = 0;
	
	jQuery(document).ready(function(){
		
		//adfit
		/* if ( window.matchMedia( '( min-width: 1024px )' ).matches == true ) {
			jQuery("#adArea").append('<ins class="kakao_ad_area" style="display:none;" data-ad-unit = "DAN-rl2dumrjsh13" data-ad-width = "728" data-ad-height = "90" data-ad-onfail="callBackFunc"></ins><script type="text/javascript" src="//t1.daumcdn.net/kas/static/ba.min.js" async><\/script>');
		}
		
		if ( window.matchMedia( '( max-width: 768px )' ).matches == true ) {
			jQuery("#adArea").append('<ins class="kakao_ad_area" style="display:none;" data-ad-unit = "DAN-trtn802noshf" data-ad-width = "300" data-ad-height = "250" data-ad-onfail="callBackFunc"></ins><script type="text/javascript" src="//t1.daumcdn.net/kas/static/ba.min.js" async><\/script>');
		} */
		
		jQuery( document ).on( 'click', '#musicListTable td[name=videoId]', function () {
			//console.log("click playVideoAt");
			//player.playVideoAt(jQuery(this).closest('tr').attr('id'));
			
			currentIndex = parseInt(jQuery(this).closest('tr').attr('id'));
			
			player.loadVideoById(videoIdList[currentIndex]);
	    	player.playVideo();
			
		});
		
		var settingDate = new Date();
		settingDate.setDate(settingDate.getDate()-1); //하루 전
		
		jQuery('.input-group.date').datepicker({
			language: "kr",
			//autoSize: true,
			//todayHighlight: true,
			startDate: "04/13/2018",
			endDate: settingDate
		});
		
		jQuery('#date').datepicker({
			language: "kr",
			//autoSize: true,
			//todayHighlight: true,
			startDate: "04/13/2018",
			endDate: settingDate
		});
		
		var regYear = (document.musicVO.regYmd.value).substring(0,4);
		var regMonth = (document.musicVO.regYmd.value).substring(4,6);
		var regDay = (document.musicVO.regYmd.value).substring(6,8);
		
		//console.log(regMonth+"/"+regDay+"/"+regYear);
		
		jQuery('#date').datepicker("setDate", new Date(regMonth+"/"+regDay+"/"+regYear));
		
		jQuery('#date').datepicker().on('changeDate', function (ev) {
			
			//console.log("changeDate : " + jQuery('#date').datepicker("getDate").getFullYear() +  pad((1 + jQuery('#date').datepicker("getDate").getMonth()),2) +  pad(jQuery('#date').datepicker("getDate").getDate(),2));
			
			document.musicVO.action = "/music";
			document.musicVO.regYmd.value = jQuery('#date').datepicker("getDate").getFullYear() +  pad((1 + jQuery('#date').datepicker("getDate").getMonth()),2) +  pad(jQuery('#date').datepicker("getDate").getDate(),2);
			
			if(document.musicVO.category.value == "R01" || document.musicVO.category.value == "R02"){
				document.musicVO.category.value = "D02";
			}else if(document.musicVO.category.value == "R03"){
				document.musicVO.category.value = "D10";
			}
			
			//console.log(document.musicVO.category.value);
			//console.log(document.musicVO.regYmd.value);
			
			document.musicVO.submit();
			
		});
		
		jQuery('#lyrics').on('click', function (ev) {
			
			jQuery("#lyricsArea").toggle();
			
		});
		
		jQuery('#download').on('click', function (ev) {	
			jQuery.ajax({
		    	url : "/music/download",
		    	data : { "videoId": currentVideoId },
		    	type : "POST",
		        dataType : "json",
		        beforeSend:function(){
		        	//openProgress();
		        },
		        success:function(data){
		        	
		        },
		        error:function(e){
		        	console.log("다운로드 에러 : " + e.responseText);
		            //closeProgress();
		        }
		    });
		});
		
		jQuery("#slider").slider({
			range: "min",
            min: 0,
            max: 100,
			value : 100,
			step : 1,
			slide : function( event, ui ){
				player.setVolume(ui.value);
				$("#range").css({ "width": ui.value + "%" }); 
			}
		});
		
		jQuery( document ).on( 'click', '.action-button a', function () {			
		    if (jQuery(this).hasClass("play-pause")) {
		    	
		    	if(pause){
		    		pause = false;
		    		player.playVideo();
		    	}else{
		    		pause = true;
		    		player.pauseVideo();
		    	}
		    	
		    	return jQuery(this).toggleClass("active");
		    }
		    
		    if (jQuery(this).hasClass("prev")) {
		    	//player.previousVideo();
		    	prevVideo();
		    }
		    
		    if (jQuery(this).hasClass("next")) {
		    	//player.nextVideo();
		    	nextVideo();
		    }
		    
		    if (jQuery(this).hasClass("repeat")) {
		    	
		    	if(repeat){
		    		repeat = false;
		    		//player.setLoop(repeat);
		    	}else{
		    		repeat = true;
		    		//player.setLoop(repeat);
		    	}
		    	
		    	return jQuery(this).toggleClass("active");
		    }
		    
		    if (jQuery(this).hasClass("random")) {
		    	
		    	shuffle();
		    	
		    	/* 
		    	if(random){
		    		random = false;
		    		player.setShuffle(random);
		    		
		    	}else{
		    		random = true;
		    		player.setShuffle(random);
		    	}		    	 
		    	
		    	return jQuery(this).toggleClass("active");
		    	
		    	*/
		    }
		    
		    if (jQuery(this).hasClass("volume")) {
		    	//alert("volume");
		    }
		
		});
		
		jQuery( document ).on( 'click', '.time-rail', function (e) {
			
			let currentTargetRect = e.currentTarget.getBoundingClientRect();
			const event_offsetX = e.pageX - currentTargetRect.left;
			
			var xPos = (100 * (event_offsetX / e.currentTarget.offsetWidth)) + "%";
			
			//console.log("e.pageX : " + e.pageX);
			//console.log("event_offsetX : " + event_offsetX);
			//console.log("e.currentTarget.offsetWidth : " + e.currentTarget.offsetWidth);
			
			jQuery('.thumb').css("left", xPos);
			
			var seekTime = player.getDuration() * (event_offsetX / e.currentTarget.offsetWidth);
			
			//console.log("event_offsetX : " + event_offsetX);
			//console.log("getDuration : " + player.getDuration());
			//console.log("(event_offsetX / e.currentTarget.offsetWidth) : " + (event_offsetX / e.currentTarget.offsetWidth));
			//console.log("seekTime : " + seekTime);
			
			player.seekTo(seekTime, true);
			
		});
		
	});
	
	function prevVideo() {
		
		if((currentIndex - 1) < 0){
    		if(repeat){
    			currentIndex = (videoIdList.length-1);
    		}else {
    			currentIndex = 0;
    		}
    	}else {
    		currentIndex = currentIndex - 1;
    	}
    	
    	player.loadVideoById(videoIdList[currentIndex]);
    	player.playVideo();
    	
	}
	
	function nextVideo() {
		
		if((currentIndex + 1) > (videoIdList.length-1)){
    		if(repeat){
    			currentIndex = 0;
    		}else {
    			currentIndex = (videoIdList.length-1);
    		}
    	}else {
    		currentIndex = currentIndex + 1;
    	}
    	
    	player.loadVideoById(videoIdList[currentIndex]);
    	player.playVideo();
		
	}
	
	function shuffle() {
		
		openProgress();
		
		dataList.sort(() => Math.random() - 0.5);
		
		var obj = jQuery("#musicListTable");
		jQuery(obj).find("tbody").remove();
		
		var tableList = [];
		
		videoIdList = [];
		
		jQuery.each(dataList, function(key, value) {
			
			//console.log('videoId: ' + value.videoId + ' title: ' + value.title + ' artist: ' + value.artist);
			
			if(value.videoId == null || value.videoId == ""){
				videoIdList[key] = "p3QRB5vXI5U";
			}else{
				videoIdList[key] = value.videoId;
			}
			
			tableList[key] = "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
			+ "<td name='videoId'>" + (key+1) + "</td>"
			+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
			+ "<td name='videoErr' onClick=\"searchChangeVideo(\'"+ key + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
			+ "<td name='addMyList' onClick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
			+ "</tr>";
			
			/* switch (key){						
			case 25 :							
				//반응형 3
				tableList[key] = "<tr>"
					+ "<td colspan='4'>"
					+ "	<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-4796638766531299' data-ad-slot='7284359859' data-ad-format='auto'></ins>"
					+ "<script>"
					+ "(adsbygoogle = window.adsbygoogle || []).push({});"
					+ "<\/script>"
					+ "</td>"
					+ "</tr>"
					+ "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
					+ "<td name='videoId'>" + (key+1) + "</td>"
					+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
					+ "<td name='videoErr'><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
					+ "<td name='addMyList' onclick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
					+ "</tr>";
			break;
			default :
				tableList[key] = "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
				+ "<td name='videoId'>" + (key+1) + "</td>"
				+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
				+ "<td name='videoErr' onClick=\"searchChangeVideo(\'"+ key + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
				+ "<td name='addMyList' onClick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
				+ "</tr>";
			} */
			
			/* switch (key){						
			case 25 :							
				//반응형 3
				tableList[key] = "<tr>"
					+ "<td colspan='4'>"
					+ "	<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-4796638766531299' data-ad-slot='7284359859' data-ad-format='auto'></ins>"
					+ "<script>"
					+ "(adsbygoogle = window.adsbygoogle || []).push({});"
					+ "<\/script>"
					+ "</td>"
					+ "</tr>"
					+ "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
					+ "<td name='videoId'>" + (key+1) + "</td>"
					+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
					+ "<td name='videoErr'><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
					+ "<td name='addMyList' onclick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
					+ "</tr>";
			break;
			case 50 :
				//반응형 4
				tableList[key] = "<tr>"
					+ "<td colspan='4'>"
					+ "	<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-4796638766531299' data-ad-slot='3490392028' data-ad-format='auto'></ins>"
					+ "<script>"
					+ "(adsbygoogle = window.adsbygoogle || []).push({});"
					+ "<\/script>"
					+ "</td>"
					+ "</tr>"
					+ "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
					+ "<td name='videoId'>" + (key+1) + "</td>"
					+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
					+ "<td name='videoErr'><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
					+ "<td name='addMyList' onClick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
					+ "</tr>";
			break;
			case 75 :
				//반응형 5
				tableList[key] = "<tr>"
					+ "<td colspan='4'>"
					+ "	<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-4796638766531299' data-ad-slot='7885890465' data-ad-format='auto'></ins>"
					+ "<script>"
					+ "(adsbygoogle = window.adsbygoogle || []).push({});"
					+ "<\/script>"
					+ "</td>"
					+ "</tr>"
					+ "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
					+ "<td name='videoId'>" + (key+1) + "</td>"
					+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
					+ "<td name='videoErr'><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
					+ "<td name='addMyList' onClick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
					+ "</tr>";
			break;
			case 99 :
				//반응형 6
				tableList[key] = "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
					+ "<td name='videoId'>" + (key+1) + "</td>"
					+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
					+ "<td name='videoErr'><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
					+ "<td name='addMyList' onClick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"					
					+ "</tr>"
					+ "<tr>"
					+ "<td colspan='4'>"
					+ "	<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-4796638766531299' data-ad-slot='2061760289' data-ad-format='auto'></ins>"
					+ "<script>"
					+ "(adsbygoogle = window.adsbygoogle || []).push({});"
					+ "<\/script>"
					+ "</td>"
					+ "</tr>";
			break;
			default :
				tableList[key] = "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
				+ "<td name='videoId'>" + (key+1) + "</td>"
				+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
				+ "<td name='videoErr' onClick=\"searchChangeVideo(\'"+ key + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
				+ "<td name='addMyList' onClick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
				+ "</tr>";
			} */
			
		});
		
		jQuery(obj).append(tableList.join(''));
		
		jQuery("[data-toggle='tooltip']").tooltip();
		
		//player.loadPlaylist(videoIdList);
		
		currentIndex = 0;			        	
        player.loadVideoById(videoIdList[currentIndex]);
        
        player.setVolume(100);
        player.playVideo();
		
		setInterval(function() {
			//console.log("onPlayerStateChange player.getVideoLoadedFraction() : " + player.getVideoLoadedFraction());
			//console.log("onPlayerStateChange player.getDuration() : " + parseInt((player.getDuration()%3600)/60) + ":" + Math.ceil(player.getDuration()%60));
			//console.log("onPlayerStateChange player.getCurrentTime() : " + parseInt((player.getCurrentTime()%3600)/60) + ":" + Math.ceil(player.getCurrentTime()%60));
			
			if(player != null){	
				//console.log("getVolume : " + player.getVolume());
				jQuery("#slider").slider("value", player.getVolume());
				jQuery('.thumb').css("left", (player.getCurrentTime() / player.getDuration() * 100) + '%');
				jQuery('.current').text(parseInt((player.getCurrentTime()%3600)/60) + ":" + pad(Math.ceil(player.getCurrentTime()%60), 2));
				jQuery('.duration').text(parseInt((player.getDuration()%3600)/60) + ":" + pad(Math.ceil(player.getDuration()%60), 2));
			}			
        }, 1000);
		
		closeProgress();
		
	}
	
	//adfit
	function callBackFunc(elm) {  
	    text = [];  
	    text.push('<ins class="adsbygoogle"');  
	    text.push('style="display:block"');  
	    text.push('data-ad-client="ca-pub-4796638766531299"');  
	    text.push('data-ad-slot="1362907193"');
	    text.push('data-ad-format="auto"');  
	    text.push('data-full-width-responsive="true"></ins>');  
	    
	    elm.innerHTML = text.join(' ');  
	    (adsbygoogle = window.adsbygoogle || []).push({});  
	}
	
	</script>

</head>
<body style="background-color:#212529;">
	<!-- Loading -->
	<div id="loadingBar">
		<div><img id="loading-image" src="/image/viewLoading.gif" alt="로딩중" /></div>
	</div>
	
	<form id="musicVO" name="musicVO" method="post">
		<input type="hidden" name="category" id="category" value="${musicVO.category}"/>
		<input type="hidden" name="regYmd" id="regYmd" value="${musicVO.regYmd}" data-date-format="YYYYMMDD" />
	</form>
	
    <div class="header-dark">
        <nav class="navbar navbar-dark navbar-expand navigation-clean-search">
            <div class="container">
            	<a class="navbar-brand" href="#" onclick="javascript:goCategoryItem('R01');">MD Music</a>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <form class="form-inline mr-auto" onsubmit="return false;">
                        <div class="form-group" id="searchForm">
                        	<label for="search-field">
                        		<i class="fa fa-search"></i>
                        	</label>
                        	<input class="form-control search-field" type="text" name="search-field" id="search-field" onkeypress="javascript:enterKey();">
                        	<!-- <button onclick="javascript:keywordSearch();">search</button> -->
                        </div>
                    </form>
                </div>
            </div>
            <%-- 
            <ul class="nav navbar-nav">
            	<li class="nav-item">
            		<img class="nav-link" id="lyrics" src="/image/lyrics_on.png" alt='가사보기' data-toggle='tooltip' title='showLyrics' data-original-title='Default tooltip' style='width:25px;height:auto;'></img>
            	</li>
                <li class="nav-item">
                	<img class="nav-link" src="/image/person.png" alt='내 목록보기' data-toggle='tooltip' title='myList' data-original-title='Default tooltip' style='width:25px;height:auto;' onclick="javascript:goCategoryItem('myList');"></img>
                </li>
                <!-- <a class="nav-link" href="#" onclick="javascript:goCategoryItem('myList');">MyList</a> -->
            </ul>
             --%>
             <button type="button" class="btn btn-link collapsed" id="lyrics">
            	<span class='sr-only'>Show Lyrics</span>
		        <span class='glyphicon glyphicon-list-alt' style="color: #fff;"></span>
            </button>
             <button type="button" class="btn btn-link collapsed" onclick="javascript:goCategoryItem('myList');">
            	<span class='sr-only'>My List</span>
		        <span class='glyphicon glyphicon-user' style="color: #fff;"></span>
            </button>
            <button type="button" class="btn btn-link navbar-toggle collapsed" data-toggle="modal" data-target="#menuModal">
            	<span class='sr-only'>Open navigation</span>
		        <span class='glyphicon glyphicon-align-justify' style="color: #fff;"></span>
            </button>
        </nav>
    </div>
    
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> -->
    <%-- <script src="/js/youtube/auth.js"></script> --%>    
    
    <!-- FULLSCREEN MODAL CODE (.fullscreen) -->
	<div class="modal fade fullscreen" id="menuModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" style="color:#fff;">
				<div class="modal-header" style="border:0;">
					<button type="button" class="close btn btn-link" data-dismiss="modal" aria-hidden="true">
						<i class="fa fa-close fa-lg" style="color:#fff;"></i>
					</button>
					<h4 class="modal-title text-center"><span class="sr-only">main navigation</span></h4>
				</div>
				<div class="modal-body text-center" id="modal-body">
					<h3 class="text-primary">실시간 TOP 100</h3>
					<ul style="list-style-type:none;">
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('R01');">All</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('R02');">Kpop</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('R03');">Pop</a></li>
					</ul>
					<h3 class="text-primary">일간 TOP 100</h3>
					<h4 class="text-success">국내</h4>
					<ul style="list-style-type:none;">
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('D02');">All</a></li>
				        <li><a class="big" href="#" onclick="javascript:goCategoryItem('D03');">Ballad/Dance/Pop</a></li>
				        <li><a class="big" href="#" onclick="javascript:goCategoryItem('D04');">HipHop</a></li>
				        <li><a class="big" href="#" onclick="javascript:goCategoryItem('D05');">R&B/Soul</a></li>
				        <li><a class="big" href="#" onclick="javascript:goCategoryItem('D06');">Electronic</a></li>
			           	<li><a class="big" href="#" onclick="javascript:goCategoryItem('D07');">Rock/Metal</a></li>
			           	<li><a class="big" href="#" onclick="javascript:goCategoryItem('D08');">Jazz</a></li>
			           	<li><a class="big" href="#" onclick="javascript:goCategoryItem('D09');">Trot</a></li>
			           	<li><a class="big" href="#" onclick="javascript:goCategoryItem('D18');">OST</a></li>
			           	<li><a class="big" href="#" onclick="javascript:goCategoryItem('D22');">Korean Classic</a></li>
			           	<li><a class="big" href="#" onclick="javascript:goCategoryItem('D23');">CCM</a></li>
			           	<li><a class="big" href="#" onclick="javascript:goCategoryItem('D24');">Children's Song</a></li>
					</ul>
					<h4 class="text-success">해외</h4>
					<ul style="list-style-type:none;">
						<li><a class="big" href="#"  onclick="javascript:goCategoryItem('D10');">All</a></li>
			           	<li><a class="big" href="#"  onclick="javascript:goCategoryItem('D11');">Pop</a></li>
			           	<li><a class="big" href="#"  onclick="javascript:goCategoryItem('D12');">HipHop</a></li>
			           	<li><a class="big" href="#"  onclick="javascript:goCategoryItem('D13');">R&B/Soul</a></li>
			           	<li><a class="big" href="#"  onclick="javascript:goCategoryItem('D14');">Electronic</a></li>
			           	<li><a class="big" href="#"  onclick="javascript:goCategoryItem('D15');">Rock</a></li>
			           	<li><a class="big" href="#"  onclick="javascript:goCategoryItem('D16');">Metal</a></li>
			           	<li><a class="big" href="#"  onclick="javascript:goCategoryItem('D17');">Jazz</a></li>
			           	<li><a class="big" href="#"  onclick="javascript:goCategoryItem('D19');">classic</a></li>
			           	<li><a class="big" href="#"  onclick="javascript:goCategoryItem('D20');">NewAge</a></li>
			           	<li><a class="big" href="#"  onclick="javascript:goCategoryItem('D21');">JPOP</a></li>
					</ul>
					<h3 class="text-primary">년도별 TOP 100</h3>
					<ul style="list-style-type:none;">
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y22');">2022</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y21');">2021</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y20');">2020</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y19');">2019</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y18');">2018</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y17');">2017</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y16');">2016</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y15');">2015</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y14');">2014</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y13');">2013</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y12');">2012</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y11');">2011</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y10');">2010</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y09');">2009</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y08');">2008</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y07');">2007</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y06');">2006</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y05');">2005</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y04');">2004</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y03');">2003</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y02');">2002</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y01');">2001</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y00');">2000</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y99');">1999</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y98');">1998</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y97');">1997</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y96');">1996</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y95');">1995</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y94');">1994</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y93');">1993</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y92');">1992</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y91');">1991</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y90');">1990</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y89');">1989</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y88');">1988</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y87');">1987</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y86');">1986</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y85');">1985</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y84');">1984</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y83');">1983</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y82');">1982</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y81');">1981</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y80');">1980</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y70');">1970년대</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y60');">1960년대</a></li>
						<li><a class="big" href="#" onclick="javascript:goCategoryItem('Y50');">1950년대</a></li>
					</ul>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.fullscreen -->
	
	<!-- searchListModal -->
	<div class="modal fade" id="addSearchListModal" tabindex="-1" role="dialog" aria-labelledby="addSearchListModal" aria-hidden="true">
		<div class="modal-dialog" role="document">			
			<div class="modal-content">
				<div class="modal-header">
					<h6 class="modal-title">검색 결과를 선택하면 MyList 에 저장됩니다.</h6>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="searchList">
				</div>
			</div>
		</div>
	</div>
	
	<!-- searchChangeListModal -->
	<div class="modal fade" id="addSearchChangeListModal" tabindex="-1" role="dialog" aria-labelledby="addSearchChangeListModal" aria-hidden="true">
		<div class="modal-dialog" role="document">			
			<div class="modal-content">
				<div class="modal-header">
					<h6 class="modal-title">검색 결과를 선택하면 영상이 교체됩니다.</h6>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="searchChangeListList">
				</div>
			</div>
		</div>
	</div>
	
	<!-- successChangeVideoModal -->
	<div class="modal fade" id="successChangeVideoModal" tabindex="-1" role="dialog" aria-labelledby="successChangeVideoModal" aria-hidden="true">
		<div class="modal-dialog" role="document">			
			<div class="modal-content">				
				<div class="modal-body">
					영상이 교체 되었습니다.
				</div>
			</div>
		</div>
	</div>
	
	<!-- successChangeVideoModal -->
	<div class="modal fade" id="successChangeVideoModal" tabindex="-1" role="dialog" aria-labelledby="successChangeVideoModal" aria-hidden="true">
		<div class="modal-dialog" role="document">			
			<div class="modal-content">				
				<div class="modal-body">
					영상 교체를 실패 하였습니다.
				</div>
			</div>
		</div>
	</div>
    
    <!-- addMyListModal -->
	<div class="modal fade" id="addMyListModal" tabindex="-1" role="dialog" aria-labelledby="addMyListModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					MyList 에 저장 되었습니다.<br>MyList 로 이동하시겠습니까?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="javascript:goCategoryItem('myList');">Go MyList</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>	
				</div>
			</div>
		</div>
	</div>
	
	<!-- deleteMyListModal -->
	<div class="modal fade" id="deleteMyListModal" tabindex="-1" role="dialog" aria-labelledby="deleteMyListModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					MyList 에서 삭제 되었습니다.
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="javascript:goCategoryItem('myList');">확인</button>
				</div>
			</div>
		</div>
	</div>    
 
    <div class="container" style="margin-top: 65px;">
    	<!-- <div class="row">
    		<div class="col-md-12">
	        	반응형 1
				<ins class="adsbygoogle"
				     style="display:block"
				     data-ad-client="ca-pub-4796638766531299"
				     data-ad-slot="3257039452"
				     data-ad-format="auto"></ins>
				<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
			</div>
        </div> -->
        <div class="row">
            <div class="col-md-12">
            	<div class="embed-responsive embed-responsive-16by9">
			    	<div id="player"></div>
			    </div>
            </div>            
        </div>
        <!-- 
		<div class="row mt-2">
			<div class="col-md-12 text-right" id="dateArea">
				<button type="button" class="btn btn-success" id="lyrics" >
           			가사
           		</button>
           		<button type="button" class="btn btn-success" data-provide="datepicker" id="date" >
           			일별 Top100
           			<span class="input-group-addon" style="padding-left: 3px;">
				    	<span class="fa fa-calendar" style="color: #fff;"></span>
				    </span>
           		</button>
			</div>
		</div>
		 -->
		<div class="row align-items-center" id="lyricsArea" style="display: none;">
        	<div class="col-md-1"></div>
        	<div class="col-md-10 border border-white">
            	<h4 class="text-white" id="lyricsTitle"></h4>
        		<p class="text-white" id="lyricsText"></p>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="row mt-2">
			<div class="col-md-12" id="adArea" align="center">
				<!-- 자동삽입1 -->
				<!-- <ins class="adsbygoogle"
					 style="display:block; text-align:center;"
					 data-ad-layout="in-article"
					 data-ad-format="fluid"
					 data-ad-client="ca-pub-4796638766531299"
					 data-ad-slot="8926267010"></ins>
				<script>
					 (adsbygoogle = window.adsbygoogle || []).push({});
				</script> -->
				
				<!-- 클릭몬 -->
				<!-- <script type="text/javascript">
					if (/android|webos|iphone|ipad|ipod|blackberry|windows phone/i.test(navigator.userAgent)){
						(function(cl,i,c,k,m,o,n){m=cl.location.protocol+c;o=cl.referrer;m+='&mon_rf='+encodeURIComponent(o);
						n='<'+i+' type="text/javascript" src="'+m+'"></'+i+'>';cl.writeln(n);
						})(document,'script','//mtab.clickmon.co.kr/pop/wp_m_320_100_js.php?PopAd=CM_M_1003067%7C%5E%7CCM_A_1068074%7C%5E%7CAdver_M_1046207&rt_ad_id_code=RTA_106265');
					}else{
						(function(cl,i,c,k,m,o,n){m=cl.location.protocol+c;o=cl.referrer;m+='&mon_rf='+encodeURIComponent(o);
						n='<'+i+' type="text/javascript" src="'+m+'"></'+i+'>';cl.writeln(n);
						})(document,'script','//tab2.clickmon.co.kr/pop/wp_ad_728_js.php?PopAd=CM_M_1003067%7C%5E%7CCM_A_1068074%7C%5E%7CAdver_M_1046207&rt_ad_id_code=RTA_106267');
					}
				</script> -->
				
				<!-- 반응형 2 -->
				<!-- <ins class="adsbygoogle"
					 style="display:block"
					 data-ad-client="ca-pub-4796638766531299"
					 data-ad-slot="1362907193"
					 data-ad-format="auto"
					 data-full-width-responsive="true"></ins>
				<script>
					 (adsbygoogle = window.adsbygoogle || []).push({});
				</script> -->
				
				<!-- <ins class="kakao_ad_area" style="display:none;width:100%;"
				data-ad-unit = "DAN-rl2dumrjsh13"
				data-ad-width = "728"
				data-ad-height = "90"
				data-ad-onfail="callBackFunc"></ins> 
				<script type="text/javascript" src="//t1.daumcdn.net/kas/static/ba.min.js" async></script> -->
				
			</div>        	
        </div>
    </div>
    
    <div style="background-size:auto;margin-bottom: 70px;">
        <div class="container">
            <div class="row">             
               	<div class="table-responsive">
               		<table class="table table-hover table-dark" id="musicListTable">
               			<%-- <caption>Music List</caption> --%>
               			<colgroup>
               				<col style="width:10%">
							<col style="width:*">
							<col style="width:10%">
							<col style="width:10%">
						</colgroup>
						<tbody>
						</tbody>
					</table>
				</div>
            </div>
        </div>
        <div class="footer-dark">
        <footer>
            <div class="container">
                <p class="copyright">Contact : maandoo.com@gmail.com<br>Copyright ⓒ 2023 Maandoo. All Rights Reserved. </p>
            </div>
        </footer>
    </div>
    </div>
    
    <div class="music-box">
    	<div class="dashboard">
    		<div class="infos">    			
    			<div class="song">
    				<span id="playerTitle"></span>
    				<small id="playerAtrist"></small>
    			</div>    			
    		</div>  		
    		<div class="player">
    			<div class="time">
    				<small class="current">0:00</small>/<small class="duration">0:00</small>
    			</div>
    			<div class="time-rail">
    				<div class="thumb"></div>
    				<div class="track"></div>    				
    			</div>
    		</div>
    		<div class="action-button">
    			<a class="play-pause">
		    		<i class="fa fa-pause"></i>
		    	</a>    			
		    	<a class="prev">
		    		<i class="fa fa-step-backward"></i>
		    	</a>		    	
		    	<a class="next">
		    		<i class="fa fa-step-forward"></i>
		    	</a>
		    	<a class="repeat">
		    		<i class="fa fa-repeat"></i>
		    	</a>
		    	<a class="random">
    				<i class="fa fa-random"></i>
    			</a>
		    	<a class="volume">
		    		<i class="fa fa-volume-up"></i>		    		
		    	</a>
		    	<div id="slider" style="display: inline;"><div id="range"></div></div>
		    </div>
		</div>
	</div>
    
    <script>
    	
	    var dataList = null;
		var videoId = "";
		var videoIdList = [];
		
		var pause = false;
		var repeat = false;
		var random = false;
		
		var player;
    	
	 	// youtube
		// 2. This code loads the IFrame Player API code asynchronously.
		var tag = document.createElement('script');
		
		tag.src = "https://www.youtube.com/iframe_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
		
		// 3. This function creates an <iframe> (and YouTube player)
		//    after the API code downloads.	
		function onYouTubeIframeAPIReady() {			
			player = new YT.Player('player', {
				height: '360',
				width: '640',
				//videoId: videoId,
				events: {
					'onReady': onPlayerReady,
					'onStateChange': onPlayerStateChange,
					'onError': onPlayerError
				}
			});
		}
		
		// 4. The API will call this function when the video player is ready.
		function onPlayerReady(event) {
			
			openProgress();
			
			//console.log("category : " + document.musicVO.category.value);
			
			if("myList" == document.musicVO.category.value){
				
				if(localStorage['mylist'] == "undefined" || localStorage['mylist'] == null || localStorage['mylist'] == "[]"){					
					alert("저장된 My List 가 없습니다.");
					closeProgress();
					window.history.back();
				}else{
					var mylist = JSON.parse(localStorage['mylist']);
					
					var obj = jQuery("#musicListTable");
					jQuery(obj).find("tbody").remove();
					
					dataList = mylist;
					var tableList = [];
					
					jQuery("#date").hide();			
					
					jQuery.each(mylist, function(key, value) {
						
						//console.log('videoId: ' + value.videoId + ' title: ' + value.title + ' artist: ' + value.artist);
						
						if(value.videoId == null || value.videoId == ""){
							videoIdList[key] = "p3QRB5vXI5U";
						}else{
							videoIdList[key] = value.videoId;
						}
						
						tableList[key] = "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
						+ "<td name='videoId'>" + (key+1) + "</td>"
						+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
						+ "<td name='videoErr' onClick=\"searchChangeVideo(\'"+ key + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
						+ "<td name='addMyList' onClick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
						+ "</tr>";
						
						/* switch (key){						
						case 25 :							
							//반응형 3
							tableList[key] = "<tr>"
								+ "<td colspan='4'>"
								+ "	<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-4796638766531299' data-ad-slot='7284359859' data-ad-format='auto'></ins>"
								+ "<script>"
								+ "(adsbygoogle = window.adsbygoogle || []).push({});"
								+ "<\/script>"
								+ "</td>"
								+ "</tr>"
								+ "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
								+ "<td name='videoId'>" + (key+1) + "</td>"
								+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
								+ "<td name='videoErr'><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
								+ "<td name='addMyList' onclick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
								+ "</tr>";
						break;
						default :
							tableList[key] = "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
							+ "<td name='videoId'>" + (key+1) + "</td>"
							+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
							+ "<td name='videoErr' onClick=\"searchChangeVideo(\'"+ key + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
							+ "<td name='addMyList' onClick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
							+ "</tr>";
						} */
						
						/* switch (key){						
						case 25 :							
							//반응형 3
							tableList[key] = "<tr>"
								+ "<td colspan='4'>"
								+ "	<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-4796638766531299' data-ad-slot='7284359859' data-ad-format='auto'></ins>"
								+ "<script>"
								+ "(adsbygoogle = window.adsbygoogle || []).push({});"
								+ "<\/script>"
								+ "</td>"
								+ "</tr>"
								+ "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
								+ "<td name='videoId'>" + (key+1) + "</td>"
								+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
								+ "<td name='videoErr'><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
								+ "<td name='addMyList' onclick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
								+ "</tr>";
						break;
						case 50 :
							//반응형 4
							tableList[key] = "<tr>"
								+ "<td colspan='4'>"
								+ "	<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-4796638766531299' data-ad-slot='3490392028' data-ad-format='auto'></ins>"
								+ "<script>"
								+ "(adsbygoogle = window.adsbygoogle || []).push({});"
								+ "<\/script>"
								+ "</td>"
								+ "</tr>"
								+ "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
								+ "<td name='videoId'>" + (key+1) + "</td>"
								+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
								+ "<td name='videoErr'><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
								+ "<td name='addMyList' onClick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
								+ "</tr>";
						break;
						case 75 :
							//반응형 5
							tableList[key] = "<tr>"
								+ "<td colspan='4'>"
								+ "	<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-4796638766531299' data-ad-slot='7885890465' data-ad-format='auto'></ins>"
								+ "<script>"
								+ "(adsbygoogle = window.adsbygoogle || []).push({});"
								+ "<\/script>"
								+ "</td>"
								+ "</tr>"
								+ "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
								+ "<td name='videoId'>" + (key+1) + "</td>"
								+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
								+ "<td name='videoErr'><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
								+ "<td name='addMyList' onClick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
								+ "</tr>";
						break;
						case 99 :
							//반응형 6
							tableList[key] = "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
								+ "<td name='videoId'>" + (key+1) + "</td>"
								+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
								+ "<td name='videoErr'><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
								+ "<td name='addMyList' onClick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"					
								+ "</tr>"
								+ "<tr>"
								+ "<td colspan='4'>"
								+ "	<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-4796638766531299' data-ad-slot='2061760289' data-ad-format='auto'></ins>"
								+ "<script>"
								+ "(adsbygoogle = window.adsbygoogle || []).push({});"
								+ "<\/script>"
								+ "</td>"
								+ "</tr>";
						break;
						default :
							tableList[key] = "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
							+ "<td name='videoId'>" + (key+1) + "</td>"
							+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
							+ "<td name='videoErr' onClick=\"searchChangeVideo(\'"+ key + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
							+ "<td name='addMyList' onClick=\"deleteMyList('"+ key + "')\"><img src='/image/minus-circular-button.png' alt='내 목록 삭제' data-toggle='tooltip' title='myList 삭제' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
							+ "</tr>";
						} */
						
					});
					
					jQuery(obj).append(tableList.join(''));
					
					jQuery("[data-toggle='tooltip']").tooltip();
					
					//player.loadPlaylist(videoIdList);
					
					currentIndex = 0;			        	
			        player.loadVideoById(videoIdList[currentIndex]);
			        
					event.target.setVolume(100);
					event.target.playVideo();
					
					setInterval(function() {
						//console.log("onPlayerStateChange player.getVideoLoadedFraction() : " + player.getVideoLoadedFraction());
						//console.log("onPlayerStateChange player.getDuration() : " + parseInt((player.getDuration()%3600)/60) + ":" + Math.ceil(player.getDuration()%60));
						//console.log("onPlayerStateChange player.getCurrentTime() : " + parseInt((player.getCurrentTime()%3600)/60) + ":" + Math.ceil(player.getCurrentTime()%60));
						
						if(player != null){	
							//console.log("getVolume : " + player.getVolume());
							jQuery("#slider").slider("value", player.getVolume());
							jQuery('.thumb').css("left", (player.getCurrentTime() / player.getDuration() * 100) + '%');
							jQuery('.current').text(parseInt((player.getCurrentTime()%3600)/60) + ":" + pad(Math.ceil(player.getCurrentTime()%60), 2));
							jQuery('.duration').text(parseInt((player.getDuration()%3600)/60) + ":" + pad(Math.ceil(player.getDuration()%60), 2));
						}			
			        }, 1000);
					
					closeProgress();
				}
				
			}else{
	        	
	        	jQuery.ajax({
			    	url : "/api",
			    	//data : jQuery("#musicVO").serialize(),
			    	//data : {"url": "/music/selectMusicList", "category": document.musicVO.category.value, "regYmd": document.musicVO.regYmd.value},
			    	data : JSON.stringify({"url": "/music/selectMusicList", "category": (document.musicVO.category.value == "" ? "R01":document.musicVO.category.value), "regYmd": document.musicVO.regYmd.value}),
			    	type : "POST",
			        dataType : "json",
			        contentType: 'application/json; charset=UTF-8',
			        beforeSend:function(xhr){
			        	//openProgress();
			        	checkTk();
			        	//console.log("getTk() : " + getTk());
			        	xhr.setRequestHeader("Authorization","Bearer " + getTk());
			        },
			        success:function(data){
			        	
			        	//onsole.log("selectMusicList success" + data);
			        	
			        	if(data == null || data == ""){
							alert("조회 결과가 없습니다.");
							closeProgress();
							window.history.back();
						}else{
							
							//console.log("selectMusicList success data.code : " + data.code);
							
							if(data.code != null && data.code != "" && data.code != "undefined"){
								alert("오류가 발생하였습니다.");
								console.log(data.errors);
							}else{
								
								var obj = jQuery("#musicListTable");
								jQuery(obj).find("tbody").remove();
								
								dataList = data;
								var tableList = [];
								
								if(document.musicVO.category.value.indexOf("Y") == 0){
									jQuery("#date").hide();							
								}else{
									jQuery("#date").show();
								}
								
								jQuery.each(data, function(key, value) {
									
									/* console.log("rank : " + value.rank);
									console.log("title : " + value.title);
									console.log("artist : " + value.artist);
									console.log("videoId : " + value.videoId); */
									
									if(value.videoId == null || value.videoId == ""){
										videoIdList[key] = "p3QRB5vXI5U";
									}else{
										videoIdList[key] = value.videoId;
										//videoIdList.push(value.videoId);
									}						
									
									tableList[key] = "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
									+ "<td name='videoId'>" + value.rank + "</td>"
									+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
									+ "<td name='videoErr' onClick=\"searchChangeVideo(\'"+ value.regNo + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
									+ "<td name='addMyList' onClick=\"addMyList(\'"+ value.videoId + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/rounded-add-button.png' alt='내 목록 저장' data-toggle='tooltip' title='myList 저장' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
									+ "</tr>";
									
									/* switch (key){						
									case 25 :							
										//반응형 3
										tableList[key] = "<tr>"
											+ "<td colspan='4'>"
											+ "	<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-4796638766531299' data-ad-slot='8003258203' data-ad-format='auto'></ins>"
											+ "<script>"
											+ "(adsbygoogle = window.adsbygoogle || []).push({});"
											+ "<\/script>"
											+ "</td>"
											+ "</tr>"
											+ "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
											+ "<td name='videoId'>" + value.rank + "</td>"
											+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
											+ "<td name='videoErr' onClick=\"searchChangeVideo(\'"+ value.regNo + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
											+ "<td name='addMyList' onClick=\"addMyList('"+ value.videoId + "','" + value.title + "','" + value.artist + "')\"><img src='/image/rounded-add-button.png' alt='내 목록 저장' data-toggle='tooltip' title='myList 저장' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
											+ "</tr>";
									break;
									default :
										tableList[key] = "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
										+ "<td name='videoId'>" + value.rank + "</td>"
										+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
										+ "<td name='videoErr' onClick=\"searchChangeVideo(\'"+ value.regNo + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
										+ "<td name='addMyList' onClick=\"addMyList(\'"+ value.videoId + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/rounded-add-button.png' alt='내 목록 저장' data-toggle='tooltip' title='myList 저장' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
										+ "</tr>";
									} */
									
									/* switch (key){						
									case 25 :							
										//반응형 3
										tableList[key] = "<tr>"
											+ "<td colspan='4'>"
											+ "	<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-4796638766531299' data-ad-slot='8003258203' data-ad-format='auto'></ins>"
											+ "<script>"
											+ "(adsbygoogle = window.adsbygoogle || []).push({});"
											+ "<\/script>"
											+ "</td>"
											+ "</tr>"
											+ "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
											+ "<td name='videoId'>" + value.rank + "</td>"
											+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
											+ "<td name='videoErr' onClick=\"searchChangeVideo(\'"+ value.regNo + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
											+ "<td name='addMyList' onClick=\"addMyList('"+ value.videoId + "','" + value.title + "','" + value.artist + "')\"><img src='/image/rounded-add-button.png' alt='내 목록 저장' data-toggle='tooltip' title='myList 저장' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
											+ "</tr>";
									break;
									case 50 :
										//반응형 4
										tableList[key] = "<tr>"
											+ "<td colspan='4'>"
											+ "	<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-4796638766531299' data-ad-slot='3490392028' data-ad-format='auto'></ins>"
											+ "<script>"
											+ "(adsbygoogle = window.adsbygoogle || []).push({});"
											+ "<\/script>"
											+ "</td>"
											+ "</tr>"
											+ "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
											+ "<td name='videoId'>" + value.rank + "</td>"
											+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
											+ "<td name='videoErr' onClick=\"searchChangeVideo(\'"+ value.regNo + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
											+ "<td name='addMyList' onClick=\"addMyList('"+ value.videoId + "','" + value.title + "','" + value.artist + "')\"><img src='/image/rounded-add-button.png' alt='내 목록 저장' data-toggle='tooltip' title='myList 저장' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
											+ "</tr>";
									break;
									case 75 :
										//반응형 5
										tableList[key] = "<tr>"
											+ "<td colspan='4'>"
											+ "	<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-4796638766531299' data-ad-slot='7885890465' data-ad-format='auto'></ins>"
											+ "<script>"
											+ "(adsbygoogle = window.adsbygoogle || []).push({});"
											+ "<\/script>"
											+ "</td>"
											+ "</tr>"
											+ "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
											+ "<td name='videoId'>" + value.rank + "</td>"
											+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
											+ "<td name='videoErr' onClick=\"searchChangeVideo(\'"+ value.regNo + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
											+ "<td name='addMyList' onClick=\"addMyList('"+ value.videoId + "','" + value.title + "','" + value.artist + "')\"><img src='/image/rounded-add-button.png' alt='내 목록 저장' data-toggle='tooltip' title='myList 저장' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
											+ "</tr>";
									break;
									case 99 :
										//반응형 6
										tableList[key] = "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
											+ "<td name='videoId'>" + value.rank + "</td>"
											+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
											+ "<td name='videoErr' onClick=\"searchChangeVideo(\'"+ value.regNo + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
											+ "<td name='addMyList' onClick=\"addMyList('"+ value.videoId + "','" + value.title + "','" + value.artist + "')\"><img src='/image/rounded-add-button.png' alt='내 목록 저장' data-toggle='tooltip' title='myList 저장' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"					
											+ "</tr>"
											+ "<tr>"
											+ "<td colspan='4'>"
											+ "	<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-4796638766531299' data-ad-slot='2061760289' data-ad-format='auto'></ins>"
											+ "<script>"
											+ "(adsbygoogle = window.adsbygoogle || []).push({});"
											+ "<\/script>"
											+ "</td>"
											+ "</tr>";
									break;
									default :
										tableList[key] = "<tr id='" + key + "' name='" + value.videoId + "' style='cursor:pointer;'>"
										+ "<td name='videoId'>" + value.rank + "</td>"
										+ "<td name='videoId'>" + value.title + "<br>" + value.artist + "</td>"
										+ "<td name='videoErr' onClick=\"searchChangeVideo(\'"+ value.regNo + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/exclamation-button.png' alt='영상교체' data-toggle='tooltip' title='영상교체' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
										+ "<td name='addMyList' onClick=\"addMyList(\'"+ value.videoId + "\',\'" + value.title + "\',\'" + value.artist + "\')\"><img src='/image/rounded-add-button.png' alt='내 목록 저장' data-toggle='tooltip' title='myList 저장' data-original-title='Default tooltip' style='width:25px;height:auto;'></td>"
										+ "</tr>";
									} */
									
								});
								
								//console.log("videoIdList : " + videoIdList);
								jQuery(obj).append(tableList.join(''));
								
								jQuery("[data-toggle='tooltip']").tooltip();
								
							}
							
						}						
			        	
			        	currentIndex = 0;			        	
			        	player.loadVideoById(videoIdList[currentIndex]);
			        	
						//player.loadPlaylist(videoIdList);
						
						/* 
						player.loadPlaylist({
			                'playlist': videoIdList,
			                'listType': 'playlist',
			                'index': 0,
			                'startSeconds': 0,
			                'suggestedQuality': 'small'
			            });
						 */
						
						//player.cuePlaylist(videoIdList);
						
						//event.target.mute();
						event.target.setVolume(100);
						event.target.playVideo();
						//event.target.unMute();
						
						
						setInterval(function() {
							//console.log("onPlayerStateChange player.getVideoLoadedFraction() : " + player.getVideoLoadedFraction());
							//console.log("onPlayerStateChange player.getDuration() : " + parseInt((player.getDuration()%3600)/60) + ":" + Math.ceil(player.getDuration()%60));
							//console.log("onPlayerStateChange player.getCurrentTime() : " + parseInt((player.getCurrentTime()%3600)/60) + ":" + Math.ceil(player.getCurrentTime()%60));
							
							if(player != null){
								//console.log("getVolume : " + player.getVolume());
								jQuery("#slider").slider("value", player.getVolume());
								jQuery('.thumb').css("left", (player.getCurrentTime() / player.getDuration() * 100) + '%');
								jQuery('.current').text(parseInt((player.getCurrentTime()%3600)/60) + ":" + pad(Math.ceil(player.getCurrentTime()%60), 2));
								jQuery('.duration').text(parseInt((player.getDuration()%3600)/60) + ":" + pad(Math.ceil(player.getDuration()%60), 2));
							}			
				        }, 1000);
						
						closeProgress();
			        
			        },
			        error:function(e){
						alert("오류가 발생하였습니다.");
						console.log(e.responseText);
			            closeProgress();
			        }
			    });
				
				
			}
			
		}
		
		// 5. The API calls this function when the player's state changes.
		//    The function indicates that when playing a video (state=1),
		//    the player should play for six seconds and then stop.
		
		var done = false;
		
		function onPlayerStateChange(event) {
			
			//console.log("onPlayerStateChange : " + event.data);
			
			/* 
			if (event.data == YT.PlayerState.PLAYING && !done) {				
				setTimeout(stopVideo, 6000);
				done = true;
			}
			 */
			 
			if(event.data == YT.PlayerState.PLAYING){
				
				//console.log("YT.PlayerState.PLAYING : " + player.getPlaylistIndex());
				/* 
				removeTrClass();
				jQuery("#"+player.getPlaylistIndex()).attr('class','table-active');
				jQuery("#playerTitle").text(dataList[player.getPlaylistIndex()].title);
				jQuery("#playerAtrist").text(dataList[player.getPlaylistIndex()].artist);
				
				jQuery("#slider").slider("value", player.getVolume());
				 */
				 
				removeTrClass();
				jQuery("#"+currentIndex).attr('class','table-active');
				jQuery("#playerTitle").text(dataList[currentIndex].title);
				jQuery("#playerAtrist").text(dataList[currentIndex].artist);				
				jQuery("#slider").slider("value", player.getVolume());
				
				currentVideoId = dataList[currentIndex].videoId;
				getCseToc(dataList[currentIndex].title, dataList[currentIndex].artist);
				
			} else if(event.data == -1){
				
				//currentVideoId = dataList[player.getPlaylistIndex()].videoId;
				//getCseToc(dataList[player.getPlaylistIndex()].title, dataList[player.getPlaylistIndex()].artist);				
				
				
				
				//console.log("videoId : " + dataList[player.getPlaylistIndex()].videoId);
				//$('#musicListTable').scrollTop(jQuery("tr[name=" + dataList[player.getPlaylistIndex()].videoId + "]").position().top);
				
			} else if(event.data == 0){
				nextVideo();
			}
			
		}
		
		function onPlayerError(event) {
			console.log("youtube api onPlayerError event.data : " + event.data);
			console.log("youtube api onPlayerError : " + JSON.stringify(event));
		}
		
		function stopVideo() {			
			player.stopVideo();
		}
		
		function removeTrClass() {		
			jQuery.each(videoIdList, function(key, value) {
				jQuery("#"+key).attr('class','');
			});		
		}
		
		function changeVideo(regNo, videoId, videoTitle, thumbnail){
			
			//console.log(regNo, videoId, thumbnail);
			
			if("myList" == document.musicVO.category.value){
				console.log("myList" + regNo, videoId, thumbnail);
				
				var mylist = JSON.parse(localStorage['mylist']);
				
				//console.log("myList " + regNo + mylist[regNo].videoId, mylist[regNo].title, mylist[regNo].artist);
				
				var title = mylist[regNo].title;
				var artist = mylist[regNo].artist;
				
				mylist.splice(regNo, 1);
				mylist.splice(regNo, 0, {"videoId": videoId, "title": title, "artist": artist});
				localStorage['mylist'] = JSON.stringify(mylist);
				
				jQuery('.modal').modal('hide');
				jQuery('#successChangeVideoModal').modal('show');
				
				setTimeout(function() {
					document.musicVO.action = "/music";
					document.musicVO.submit();
				}, 1000);
				
			}else{
				jQuery.ajax({
			    	url : "/api",
			    	data : JSON.stringify({ "url": "/music/changeVideo", "regNo": regNo, "videoId": videoId, "videoTitle": videoTitle, "thumbnail": thumbnail, "category": document.musicVO.category.value }),
			    	type : "POST",
			        dataType : "json",
			        contentType: 'application/json; charset=UTF-8',
			        beforeSend:function(xhr){
			        	//openProgress();
			        	checkTk();
			        	//console.log("getTk() : " + getTk());
			        	xhr.setRequestHeader("Authorization","Bearer " + getTk());
			        },
			        success:function(data){
			        	
			        	if(data == null || data == ""){
							console.log("결과 값 없음");
						}else{
							
							if(data){
								
								if(data.code != null && data.code != "" && data.code != "undefined"){
									alert("오류가 발생하였습니다.");
									console.log(data.errors);
								}else{
									
									//console.log("영상교체 성공");
									
									jQuery('.modal').modal('hide');
									
									jQuery('#successChangeVideoModal').modal('show');
									
									setTimeout(function() {
										document.musicVO.action = "/music";
										document.musicVO.submit();
									}, 1000);
									
								}
								
				        	}else{
				        		//console.log("영상교체 실패");
				        		
								jQuery('.modal').modal('hide');
								
								jQuery('#failChangeVideoModal').modal('show');
				        	}
						}	        	
			        	
			        },
			        error:function(e){
			        	console.log("영상교체 에러 : " + e.responseText);
			            //closeProgress();
			        }
			    });
			}
			
		}
		
		function addMyList(videoId, title, artist) {
			
			if(typeof(Storage) !== "undefined") {
	
				if(localStorage['mylist'] == null){
					
					localStorage['mylist'] = JSON.stringify([{"videoId": videoId, "title": title, "artist": artist}]);
					
					jQuery('.modal').modal('hide');					
					jQuery('#addMyListModal').modal({backdrop: 'static', keyboard: false}, 'show');
					
				}else{
					
					var mylist = JSON.parse(localStorage['mylist']);					
					//console.log("mylist.length : " + mylist.length);
					
					mylist.push({"videoId": videoId, "title": title, "artist": artist});
					localStorage['mylist'] = JSON.stringify(mylist);
					
					jQuery('.modal').modal('hide');						
					jQuery('#addMyListModal').modal({backdrop: 'static', keyboard: false}, 'show');
					
				}
	
			} else {
	
			    alert("해당 브라우져에서는 MyList 기능을 지원하지 않습니다.");
	
			}
			
		}
		
		function deleteMyList(index) {
			
			//console.log('index: ', index);
			
			//console.log('mylist1: ', JSON.parse(localStorage['mylist']));
			
			var mylist = JSON.parse(localStorage['mylist']);
			
			mylist.splice(index, 1);
			localStorage['mylist'] = JSON.stringify(mylist);
			
			jQuery('#deleteMyListModal').modal({backdrop: 'static', keyboard: false}, 'show');
			
			//console.log('mylist: ', JSON.parse(localStorage['mylist']));
			
		}
		
		function goCategoryItem(category){
			
			if("myList" == category){
				if(typeof(Storage) !== "undefined") {
					document.musicVO.action = "/music";
					document.musicVO.category.value = category;
					document.musicVO.regYmd.value = "";
					document.musicVO.submit();
				} else {
				    alert("해당 브라우져에서는 MyList 기능을 지원하지 않습니다.");
				}
			}else{
				document.musicVO.action = "/music";
				document.musicVO.category.value = category;
				document.musicVO.regYmd.value = "";
				document.musicVO.submit();
			}
			
		}
		
		function enterKey(){
			if(event.keyCode == 13){
				keywordSearch();
				return;
			}
		}
		
		function getCseToc(currentTitle, currentArtist){
			jQuery.ajax({
		    	url : "/api",
		    	data : JSON.stringify({"url": "/music/selectCseToc"}),
		    	type : "POST",
		    	dataType : "text",
		    	contentType: 'application/json; charset=UTF-8',
		        async : false,
		        beforeSend:function(xhr){
		        	checkTk();
		        	xhr.setRequestHeader("Authorization","Bearer " + getTk());
		        	jQuery("#lyricsTitle,#lyricsText").empty();
					jQuery("#lyricsTitle").append("가사 검색 중...");
					jQuery("#lyricsText").append("");
		        },
		        success:function(data){		        	
		        	
		        	if(data == null || data == ""){
						console.log("가사 검색 결과가 없습니다.");
						jQuery("#lyricsTitle,#lyricsText").empty();
						jQuery("#lyricsTitle").append("가사 검색 결과가 없습니다.");
						jQuery("#lyricsText").append("");
						closeProgress();
					}else{
						if(data.code != null && data.code != "" && data.code != "undefined"){
							// alert("가사 검색 중 오류가 발생하였습니다.");
							console.log(data.errors);
							jQuery("#lyricsTitle,#lyricsText").empty();
							jQuery("#lyricsTitle").append("가사 검색 결과가 없습니다.");
							jQuery("#lyricsText").append("");
				            closeProgress();
						}else{
							searchLyrics(currentTitle, currentArtist, data);
						}						
					}
		        
		        },
		        error:function(e){
		        	console.log(e.responseText);    	
		        	//alert("가사 검색 중 오류가 발생하였습니다.");
		        	jQuery("#lyricsTitle,#lyricsText").empty();
					jQuery("#lyricsTitle").append("가사 검색 결과가 없습니다.");
					jQuery("#lyricsText").append("");
		            closeProgress();
		        }
		    });
		}
		
		function searchLyrics(currentTitle, currentArtist, cseTok){
			jQuery.ajax({
		    	url : "https://cse.google.com/cse/element/v1",
		    	data : {
		    		num: "1",
		    		hl: "ko",
		    		cx: "007672770734803526218:21emw7atxz4",
		    		q: currentTitle + " " +currentArtist,
		    		safe: "off",
		    		cse_tok: cseTok
		    	},
		    	type : "GET",
		        dataType : "jsonp",
		        async : false,
		        beforeSend:function(){
		        	jQuery("#lyricsTitle,#lyricsText").empty();
					jQuery("#lyricsTitle").append("가사 검색 중...");
					jQuery("#lyricsText").append("");
		        },
		        success:function(data){
		        	
		        	//console.log(data);		        	
		        	
		        	if(data.results == null || data.results == ""){
		        		//alert("가사 검색 결과가 없습니다.");
		        		jQuery("#lyricsTitle,#lyricsText").empty();
		        		jQuery("#lyricsTitle").append("가사 검색 결과가 없습니다.");
						jQuery("#lyricsText").append("");
						closeProgress();
					}else{
						//console.log("data.results[0].unescapedUrl : " + data.results[0].unescapedUrl);
						getLyrics(currentTitle, currentArtist, data.results[0].unescapedUrl);
					}
		        
		        },
		        error:function(e){
					//alert("가사 검색 중 오류가 발생하였습니다.");
					
					jQuery("#lyricsTitle,#lyricsText").empty();
					jQuery("#lyricsTitle").append("가사 검색 결과가 없습니다.");
					jQuery("#lyricsText").append("");
					
					console.log(e.responseText);
					closeProgress();
		        }
		    });
		}
		
		function getLyrics(currentTitle, currentArtist, searchResult){
			
			//console.log(searchResult);
			
			jQuery.ajax({
		    	url : "/api",
		    	data : JSON.stringify({"url": "/music/selectLyrics", "searchResult" : searchResult}),
		    	type : "POST",
		    	dataType : "text",
		    	contentType: 'application/json; charset=UTF-8',
		        async : false,
		        beforeSend:function(xhr){
		        	checkTk();
		        	xhr.setRequestHeader("Authorization","Bearer " + getTk());
		        	jQuery("#lyricsTitle,#lyricsText").empty();
					jQuery("#lyricsTitle").append("가사 검색 중...");
					jQuery("#lyricsText").append("");
		        },
		        success:function(data){		        	
		        	
		        	if(data == null || data == ""){
						//console.log("가사 검색 결과가 없습니다.");
						jQuery("#lyricsTitle,#lyricsText").empty();
						jQuery("#lyricsTitle").append("가사 검색 결과가 없습니다.");
						jQuery("#lyricsText").append("");
					}else{
						if(data.code != null && data.code != "" && data.code != "undefined"){
							console.log(data.errors);
							//alert("가사 검색 중 오류가 발생하였습니다.");
						}else{
							jQuery("#lyricsTitle,#lyricsText").empty();
							jQuery("#lyricsTitle").append(currentTitle + " - " + currentArtist);
							jQuery("#lyricsText").append(data);	
						}			
					}				
					
					closeProgress();
		        
		        },
		        error:function(e){
		        	console.log(e.responseText);    	
		        	//alert("가사 검색 중 오류가 발생하였습니다.");
		        	jQuery("#lyricsTitle,#lyricsText").empty();
					jQuery("#lyricsTitle").append("가사 검색 결과가 없습니다.");
					jQuery("#lyricsText").append("");
		            closeProgress();
		        }
		    });
		}
    
    </script>
    
    <script src="/js/youtube/search.js"></script>
    <script src="https://apis.google.com/js/client.js?onload=googleApiClientReady"></script>
    
    <!-- 클릭몬 전면 -->
    <!-- <script type="text/javascript">
	    if (/android|webos|iphone|ipad|ipod|blackberry|windows phone/i.test(navigator.userAgent)){
	    	(function(cl,i,c,k,m,o,n){m=cl.location.protocol+c;o=cl.referrer;m+='&mon_rf='+encodeURIComponent(o);
			n='<'+i+' type="text/javascript" src="'+m+'"></'+i+'>';cl.writeln(n);
			})(document,'script','//mtab.clickmon.co.kr/pop/wp_m_pop.php?PopAd=CM_M_1003067%7C%5E%7CCM_A_1068074%7C%5E%7CAdver_M_1046207&rt_ad_id_code=RTA_106266&iveiw=no');
	    }
	</script> -->
</body>
</html>